package es.cursame.webApp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import es.cursame.webApp.Service.UserService;

@SpringBootTest
@AutoConfigureMockMvc
public class UserRegisterLoginTests {
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private UserService userService;
	
	@Test
	public void ShouldRegister() throws Exception {
		mockMvc
			.perform(post("/registration")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.content(buildUrlEncodedFormEntity(
						"username", "testtest",
						"email", "test@test.test",
						"password", "asdasdasd",
						"passwordConfirm", "asdasdasd",
						"terms", "agree"
						)))
			.andExpect(status().isFound())
			.andExpect(authenticated().withUsername("testtest").withRoles("USER"));
		assertThat(userService.getUserByUsername("testtest")).isNotNull();
		
	}
	
	@Test
	public void ShouldLoginUser() throws Exception {
		mockMvc
			.perform(formLogin().user("cursame").password("cursame"))
			.andExpect(status().isFound())
			.andExpect(authenticated().withUsername("cursame").withRoles("USER"));
		
	}
	
	@Test
	public void ShouldLoginAdmin() throws Exception {
		mockMvc
			.perform(formLogin().user("admin").password("admin"))
			.andExpect(status().isFound())
			.andExpect(authenticated().withUsername("admin").withRoles("ADMIN"));
		
	}
	
	private String buildUrlEncodedFormEntity(String... params) {
	    if( (params.length % 2) > 0 ) {
	       throw new IllegalArgumentException("Need to give an even number of parameters");
	    }
	    StringBuilder result = new StringBuilder();
	    for (int i=0; i<params.length; i+=2) {
	        if( i > 0 ) {
	            result.append('&');
	        }
	        try {
	            result.
	            append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
	            append('=').
	            append(URLEncoder.encode(params[i+1], StandardCharsets.UTF_8.name()));
	        }
	        catch (UnsupportedEncodingException e) {
	            throw new RuntimeException(e);
	        }
	    }
	    return result.toString();
	 }
	
}
