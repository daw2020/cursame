package es.cursame.webApp;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import es.cursame.webApp.Controller.CategoryController;
import es.cursame.webApp.Controller.CourseController;
import es.cursame.webApp.Controller.LevelController;
import es.cursame.webApp.Controller.LoginController;
import es.cursame.webApp.Controller.MainController;
import es.cursame.webApp.Controller.PathController;
import es.cursame.webApp.Controller.PlatformController;

@SpringBootTest
public class SmokeTest {
	@Autowired
	private CategoryController categoryController;
	@Autowired
	private CourseController courseController;
	@Autowired
	private LevelController levelController;
	@Autowired
	private LoginController loginController;
	@Autowired
	private MainController mainController;
	@Autowired
	private PathController pathController;
	@Autowired
	private PlatformController platformController;
	
	@Test
	public void categoryControllerExists() throws Exception {
		assertThat(categoryController).isNotNull();
	}
	@Test
	public void courseControllerExists() throws Exception {
		assertThat(courseController).isNotNull();
	}
	@Test
	public void levelControllerExists() throws Exception {
		assertThat(levelController).isNotNull();
	}
	@Test
	public void loginControllerExists() throws Exception {
		assertThat(loginController).isNotNull();
	}
	@Test
	public void mainControllerExists() throws Exception {
		assertThat(mainController).isNotNull();
	}
	@Test
	public void pathControllerExists() throws Exception {
		assertThat(pathController).isNotNull();
	}
	@Test
	public void platformControllerExists() throws Exception {
		assertThat(platformController).isNotNull();
	}
}
