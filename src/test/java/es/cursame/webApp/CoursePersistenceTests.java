package es.cursame.webApp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import es.cursame.webApp.Domain.*;
import es.cursame.webApp.Repository.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class CoursePersistenceTests {	

	@Autowired
	private CourseJpaRepository courseJpaRepository;

	@Test
	public void testJpaFind() {		
		List<String> remarks = new ArrayList<String>();
		remarks.add("Es un cursazo");
		Course course = new Course();
		course.setTitle("Introducción a JAVA");
		course.setDescription("Curso introductorio a JAVA");
		course.setUrl("https://estoesunenlacealcurso.com");
		course.setImageUrl("https://estoesunenlacealcurso.com");
		course.setLevel(Level.BEGINNER);				
		
		courseJpaRepository.save(course);
	
		List<Course> courses = courseJpaRepository.findAll();

		assertTrue(courses.contains(course));
	}
	
	@Test
	@Transactional
	public void testSaveAndGetAndDelete() throws Exception {
		List<String> remarks = new ArrayList<String>();
		remarks.add("Es un cursazo");
		Course course = new Course();
		course.setTitle("Introducción a JAVA");
		course.setDescription("Curso introductorio a JAVA");
		course.setUrl("https://estoesunenlacealcurso.com");
		course.setImageUrl("https://estoesunenlacealcurso.com");
		course.setLevel(Level.BEGINNER);				
		
		courseJpaRepository.save(course);
				
		assertEquals("Introducción a JAVA", course.getTitle());
		assertEquals(Level.BEGINNER, course.getLevel());
		
		//delete BC location now
		courseJpaRepository.delete(course);
	}
	
}
