package es.cursame.webApp;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;


import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Domain.Course;
import es.cursame.webApp.Domain.Level;
import es.cursame.webApp.Domain.Path;
import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Service.CategoryService;
import es.cursame.webApp.Service.CourseService;
import es.cursame.webApp.Service.PathService;
import es.cursame.webApp.Service.PlatformService;

@SpringBootTest
@AutoConfigureMockMvc
public class MainControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CourseService courseService;
	
	@MockBean
	private CategoryService categoryService;
	
	@MockBean
	private PathService pathService;
	
	@MockBean
	private PlatformService platformService;
	
	@Test
	public void welcome_ShouldAddEntitiesToModelAndRenderIndexView() throws Exception {
		Category cat1 = new Category("Test Category", "Test");
		Category cat2 = new Category("Another Category", "another");
		Platform p1 = new Platform("Udemy", "http://udemy.com", "/assets/img/logos/logo-udemy.svg", "Udemy");
		Course c1 = new Course(
			"Diseño para principiantes",
			"Curso para beginners en diseño web",
			"https://url.es",
			"/assets/img/courses/1.jpg",
			false,
			4f,
			Level.BEGINNER,
			cat1,
			p1
		);
		Course c2 = new Course(
			"JSP for dummies",
			"Curso de nivel intermedio de Spring con JSP",
			"https://url.es",
			"/assets/img/courses/2.jpg",
			false,
			4f,
			Level.INTERMEDIATE,
			cat1,
			p1
		);
		Course c3 = new Course(
			"Demo",
			"Demo",
			"https://url.es",
			"/assets/img/courses/3.jpg",
			false,
			4f,
			Level.BEGINNER,
			cat2,
			p1
		);
		Path path1 = new Path(
				"Programación básica",
				"Para aprender a programar",
				"/assets/img/categories/1.jpg");
		path1.getCourses().add(c1);
		when(courseService.getAllCourses()).thenReturn(Arrays.asList(c1, c2, c3));
		when(categoryService.getAll()).thenReturn(Arrays.asList(cat1, cat2));
		when(pathService.getAllPaths()).thenReturn(Arrays.asList(path1));
		when(platformService.getAll()).thenReturn(Arrays.asList(p1));
		
		mockMvc.perform(get("/"))
			.andExpect(status().isOk())
			.andExpect(view().name("index"))
			.andExpect(forwardedUrl("/WEB-INF/templates/main.jsp"))
			.andExpect(model().attribute("courses", hasSize(3)))
			.andExpect(model().attribute("courses", hasItem(
					allOf(
							hasProperty("title", is("Diseño para principiantes")),
							hasProperty("description", is("Curso para beginners en diseño web")),
							hasProperty("url", is("https://url.es")),
							hasProperty("imageUrl", is("/assets/img/courses/1.jpg")),
							hasProperty("paymentRequired", is(false)),
							hasProperty("duration", is(4f)),
							hasProperty("level", is(Level.BEGINNER)),
							hasProperty("category", is(cat1)),
							hasProperty("platform", is(p1))
					)
			)))
			.andExpect(model().attribute("courses", hasItem(
					allOf(
							hasProperty("title", is("JSP for dummies")),
							hasProperty("description", is("Curso de nivel intermedio de Spring con JSP")),
							hasProperty("url", is("https://url.es")),
							hasProperty("imageUrl", is("/assets/img/courses/2.jpg")),
							hasProperty("paymentRequired", is(false)),
							hasProperty("duration", is(4f)),
							hasProperty("level", is(Level.INTERMEDIATE)),
							hasProperty("category", is(cat1)),
							hasProperty("platform", is(p1))
					)
			)))
			.andExpect(model().attribute("courses", hasItem(
					allOf(
							hasProperty("title", is("Demo")),
							hasProperty("description", is("Demo")),
							hasProperty("url", is("https://url.es")),
							hasProperty("imageUrl", is("/assets/img/courses/3.jpg")),
							hasProperty("paymentRequired", is(false)),
							hasProperty("duration", is(4f)),
							hasProperty("level", is(Level.BEGINNER)),
							hasProperty("category", is(cat2)),
							hasProperty("platform", is(p1))
					)
			)));
	}
	
}
