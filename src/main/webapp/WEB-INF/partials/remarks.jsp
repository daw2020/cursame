<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="row">
	<div class="col-lg-10 offset-lg-1 course-list">
		<div class="cl-item" id="curso-descripcion">
			<h4>Descripci�n del curso</h4>
			<p><c:out value="${course.getDescription()}" /></p>
			<div class="course-reviews">
				<h4 id="opiniones">Opiniones</h4>
				<ul class="course-reviews-list list-unstyled">
					<c:forEach items="${ course.getRemarks() }" var="review">
						<li class="course-review">
							<div class="ca-pic set-bg" data-setbg="/assets/img/usuario.jpg"></div>
							<h6><c:out value="${ review.getUser().getUsername() }" /></h6>
							<span class="rating">
								<c:choose>
									<c:when test="${ review.getScore() == 1 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>
									<c:when test="${ review.getScore() == 2 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>
									<c:when test="${ review.getScore() == 3 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>										
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>
									<c:when test="${ review.getScore() == 4 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>									
									<c:otherwise>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
									</c:otherwise>
								</c:choose>								
							</span>
							<p><c:out value="${ review.getBody() }" /></p>
						</li>
					</c:forEach>
				</ul>
				<a href="#" class="site-btn buy-btn load-more">Ver m�s</a>
	              <h5>�Quieres escribir una opini�n? <span class="add-review"><i class="fa fa-plus-circle"></i></span></h5>
	              <spring:url value="/courses/${ course.getCourseId() }/remarks" var="remarks" htmlEscape="true" />              
	              <form:form modelAttribute="remark" action="${ remarks }" class="contact-form review-form" method="post">
	              	<form:input id="course" path="course" type="hidden" />
	              	<form:input id="user" path="user" type="hidden" />
	              	<form:input id="platform" path="platform" type="hidden" />
	                <div class="rating-container">
	                  <label for="score">Tu valoraci�n:</label>
	                  <span class="rating" data-valoracion="0">
	                    <span data-score="1"><i class="fa fa-star is-fade"></i></span>
	                    <span data-score="2"><i class="fa fa-star is-fade"></i></span>
	                    <span data-score="3"><i class="fa fa-star is-fade"></i></span>
	                    <span data-score="4"><i class="fa fa-star is-fade"></i></span>
	                    <span data-score="5"><i class="fa fa-star is-fade"></i></span>
	                  </span>
	                  <form:input id="score" path="score" class="stars-control" style="display:none" type="number" />	
	                </div>
	                <label for="body">Tu comentario:</label>
	                <form:textarea id="body" path="body"></form:textarea>			                
	                <input type="submit" id="btnSaveRemark" class="save-review site-btn buy-btn" value ="Enviar"/>
	              </form:form>	
            </div>
		</div>
	</div>
</div>

