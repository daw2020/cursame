<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!-- search section -->
<spring:url value="/search" var="searchUrl" htmlEscape="true" />
<section class="search-section spad" id="buscador">
    <div class="container">
        <div class="search-warp">
            <div class="section-title text-white">
                <h2>Busca tu curso</h2>
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <!-- search form -->
                    <form:form modelAttribute="searchForm" class="course-search-form" method="POST" action="${ searchUrl }">
                        <label for="search--name" class="sr-only">Nombre:</label>
                        <form:input path="name" type="text" placeholder="Nombre" id="search--name" />
                        <label for="search--path" class="sr-only">Camino:</label>
                        <form:input path="path" type="text" class="last-m" placeholder="Camino" id="search--path" />
                        <button class="site-btn submit-busqueda-avanzada">Busca curso</button>
                        <a class="toggle-busqueda toggle-busqueda-avanzada" href="#">B�squeda avanzada</a>
                        <div class="campos-avanzado d-none">
                            <label for="search--level" class="sr-only">Nivel:</label>
                            <form:select path="level" id="search--level" class="form-control form-select">
                                <form:option value="">Selecciona nivel</form:option>
                                <c:forEach items="${ levels }" var="level">
                                    <form:option value="${ level.name() }" title="${ level.name() }" />
                                </c:forEach>
                            </form:select>
                            <label for="search--platform" class="sr-only">Plataforma:</label>
                            <form:select path="platform" id="search--platform" class="form-control form-select">
                            	<form:option value="">Selecciona plataforma</form:option>
                            	<form:options items="${ platforms }" itemValue="platformId" itemLabel="name" />
                            </form:select>
                            <label for="search--category" class="sr-only">Categor�a:</label>
                            <form:select path="category" id="search--category" class="form-control form-select">
                            	<form:option value="">Selecciona categor�a</form:option>
                            	<form:options items="${ categories }" itemValue="categoryId" itemLabel="name" />
                            </form:select>
                        </div>
                        <a class="toggle-busqueda toggle-busqueda-simple d-none" href="#">b�squeda simple</a>
                        <button class="site-btn submit-busqueda-simple d-none">Busca curso</button>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- search section end -->

