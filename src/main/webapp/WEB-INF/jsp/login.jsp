<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content=""><!-- ToDo: Rellenar la descripci�n a partir de los textos del copy -->

  <!-- Favicon -->
  <link href="/assets/img/logo.png" rel="shortcut icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/admin/AdminLTE.css">

  <title>C�rsame - Iniciar sesi�n</title>
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <div class="site-logo">
        <a href="<spring:url value= '/'/>"><img src="/assets/img/logo.svg" alt="C�rsame"></a>
        <h1><a href="<spring:url value= '/'/>">C�rsame</a></h1>
      </div>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Inicia sesi�n</p>
        <form action="<spring:url value='/login' />" method="post">   
          <div class="input-group mb-3">
            <label for="inputUser" class="sr-only">Usuario:</label>
            <input type="text" class="form-control" placeholder="Usuario" name="username" id="inputUser">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <label for="inputPassword" class="sr-only">Contrase�a:</label>
            <input type="password" class="form-control" placeholder="Contrase�a" name="password" id="inputPassword">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-7 col-sm-8">
              <div class="icheck-primary">
                <input type="checkbox" id="remember">
                <label for="remember">
                  Recu�rdame
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-5 col-sm-4">
              <button type="submit" class="btn btn-primary btn-block">Acceder</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
        <p class="mb-1">
          <a href="<spring:url value= '/forgotPassword'/>">He olvidado mi contrase�a</a>
        </p>
        <p class="mb-0">
          <a href="<spring:url value= '/registration'/>" class="text-center">Quiero registrarme</a>
        </p>
      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->

</body>
</html>