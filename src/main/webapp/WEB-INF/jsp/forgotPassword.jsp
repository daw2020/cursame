<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content=""><!-- ToDo: Rellenar la descripci�n a partir de los textos del copy -->

  <!-- Favicon -->
  <link href="/assets/img/logo.png" rel="shortcut icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/admin/AdminLTE.css">

  <title>C�rsame - Recordar contrase�a</title>
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <div class="site-logo">
        <a href="<spring:url value= '/'/>"><img src="/assets/img/logo.svg" alt="C�rsame"></a>
        <h1><a href="<spring:url value= '/'/>">C�rsame</a></h1>	        
    </div>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Introduce la direcci�n de email con la que est�s registrado y te enviaremos un correo para restablecer tu contrase�a.</p>

      <form action="/forgotPassword" method="post">
        <div class="input-group mb-3">
          <label for="email" class="sr-only">Email:</label>
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Restablecer contrase�a</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-3 mb-1">
        <a href="<spring:url value= '/login'/>">Acceder</a>
      </p>
      <p class="mb-0">
          <a href="<spring:url value= '/registration'/>" class="text-center">Quiero registrarme</a>
        </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>

</body>
</html>