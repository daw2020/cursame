<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:url value="/" var="rootUrl" htmlEscape="true" />
<spring:url value="/userProfile" var="profile" htmlEscape="true" />
<spring:url value="/privacy" var="privacyUrl" htmlEscape="true" />
<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content=""><!-- ToDo: Rellenar la descripci�n a partir de los textos del copy -->

  <!-- Favicon -->
  <link href="img/logo.png" rel="shortcut icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400&display=swap" rel="stylesheet">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/css/font-awesome.min.css" />
  <link rel="stylesheet" href="/assets/css/owl.carousel.min.css" />
  <link rel="stylesheet" href="/assets/css/style.css" />

  <!-- ToDo: poner el nombre del curso din�mico -->
  <title>C�rsame - No encontrado</title>
</head>

<body>
  <!-- Page Preloder -->
  <div id="preloder">
    <div class="loader"></div>
  </div>

  <!-- Header section -->
  <header class="header-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3">
          <div class="site-logo">
            <a href="${ rootUrl }"><img src="/assets/img/logo.svg" class="logo" alt="C�rsame"></a>
            <h1><a href="${ rootUrl }">C�rsame</a></h1>
          </div>
          <div class="nav-switch">
            <i class="fa fa-bars"></i>
          </div>
        </div>
        <div class="col-lg-9 col-md-9">
          <c:choose>
            <c:when test="${ isAuthenticated }">
	      	  <a class="nav-link dropdown-toggle site-btn header-btn" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	            <span class="mr-2 d-none d-lg-inline text-gray-600">${ userDetails.getUsername() }</span>
	            <!-- <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"> -->
	          </a>
	          <!-- Dropdown - User Information -->
	          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" style="min-width: 196px;" aria-labelledby="userDropdown">
	            <a class="dropdown-item" href="${ profile }">
	              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
	              Perfil
	            </a>	             
	            <div class="dropdown-divider"></div>
	            <a onclick="document.forms['logoutForm'].submit()" class="dropdown-item">
	              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
	              Cerrar sesi�n
	            </a>
               	<form id="logoutForm" method="POST" action="${contextPath}/logout">
            		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        		</form>
	          </div>
        	</c:when>
        	<c:otherwise>
        		<a href="${ loginUrl }" class="site-btn header-btn">Acceder</a>        		
        	</c:otherwise>        	       
        </c:choose>        
         <nav class="main-menu">
           <ul>
             <li><a href="${ rootUrl }#buscador">Buscador</a></li>
             <li><a href="${ rootUrl }#caminos">Caminos</a></li>
             <li><a href="${ rootUrl }#cursos">Cursos</a></li>
             <li><a href="${ rootUrl }#contacto">Contacto</a></li>
             <c:if test="${ !isAuthenticated }">
             	<li><a href="${ rootUrl }#registro">Registro</a></li>
             </c:if>
           </ul>
         </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- Header section end -->

  <!-- Page info -->
	<div class="page-info-section page-404 set-bg" data-setbg="/assets/img/404.jpg">
		<div class="container">

      <div class="hero-text text-white">
        <h2>No encontrado</h2>
        <p>No hemos encontrado lo que buscas. Quiz�s quieras consultar la <a href="#">p�gina principal</a> o utilizar nuestro buscador de cursos.</p>
      </div>
		</div>
	</div>
  <!-- Page info end -->

    <!-- search section -->
    <section class="search-section spad" id="buscador">
      <div class="container">
        <div class="search-warp">
          <div class="section-title text-white">
            <h2>Busca tu curso</h2>
          </div>
          <div class="row">
            <div class="col-md-10 offset-md-1">
              <!-- search form -->
              <form class="course-search-form">
                <label for="tecnologia" class="sr-only">Tecnolog�a:</label>
                <input type="text" placeholder="Tecnolog�a" id="tecnologia">
                <label for="camino" class="sr-only">Camino:</label>
                <input type="text" class="last-m" placeholder="Camino" id="camino">
                <button class="site-btn submit-busqueda-avanzada">Busca curso</button>
                <a class="toggle-busqueda toggle-busqueda-avanzada" href="#">b�squeda avanzada</a>
                <div class="campos-avanzado d-none">
                  <label for="nivel" class="sr-only">Nivel:</label>
                  <select id="nivel" class="form-control form-select">
                    <option value="">Selecciona nivel</option>
                    <option value="">Iniciaci�n</option>
                    <option value="">Medio</option>
                    <option value="">Avanzado</option>
                  </select>
                  <label for="plataforma" class="sr-only">Plataforma:</label>
                  <select id="plataforma" class="form-control form-select">
                    <option value="">Selecciona plataforma</option>
                    <option value="">Coursera</option>
                    <option value="">YouTube</option>
                    <option value="">Udemy</option>
                  </select>
                  <label for="categoria" class="sr-only">Categor�a:</label>
                  <select id="categoria" class="form-control form-select">
                    <option value="">Selecciona categor�a</option>
                    <option value="">Categor�a 1</option>
                    <option value="">Categor�a 2</option>
                    <option value="">Categor�a 3</option>
                  </select>
                </div>
                <a class="toggle-busqueda toggle-busqueda-simple d-none" href="#">b�squeda simple</a>
                <button class="site-btn submit-busqueda-simple d-none">Busca curso</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- search section end -->

  	<!-- Page -->
	<section class="realated-courses spad">
		<div class="course-warp">
			<h2 class="rc-title">Cursos destacados</h2>
			<div class="rc-slider owl-carousel">
				<!-- course -->
				<div class="course-item">
          <div class="course-item">
            <div class="course-thumb set-bg" data-setbg="/assets/img/courses/1.jpg">
              <div class="price"><a href="#">Avanzado</a></div>
              <a href="#" title="Nombre del curso"></a>
            </div>
            <div class="course-info">
              <div class="course-text">
                <h5><a href="#">Nombre del curso</a></h5>
                <p>Lorem ipsum dolor sit amet, consectetur</p>
                <div class="students"><a href="#">Categor�a</a></div>
              </div>
              <div class="course-author">
                <div class="ca-pic set-bg" data-setbg="/assets/img/logos/logo-udemy.svg"><a href="#" title="Udemy"></a></div>
                <p><a href="#">Udemy</a></p>
              </div>
            </div>
          </div>
        </div>
				<!-- course -->
				<div class="course-item">
          <div class="course-item">
            <div class="course-thumb set-bg" data-setbg="/assets/img/courses/1.jpg">
              <div class="price"><a href="#">Avanzado</a></div>
              <a href="#" title="Nombre del curso"></a>
            </div>
            <div class="course-info">
              <div class="course-text">
                <h5><a href="#">Nombre del curso</a></h5>
                <p>Lorem ipsum dolor sit amet, consectetur</p>
                <div class="students"><a href="#">Categor�a</a></div>
              </div>
              <div class="course-author">
                <div class="ca-pic set-bg" data-setbg="/assets/img/logos/logo-udemy.svg"><a href="#" title="Udemy"></a></div>
                <p><a href="#">Udemy</a></p>
              </div>
            </div>
          </div>
        </div>
				<!-- course -->
				<div class="course-item">
          <div class="course-item">
            <div class="course-thumb set-bg" data-setbg="/assets/img/courses/1.jpg">
              <div class="price"><a href="#">Avanzado</a></div>
              <a href="#" title="Nombre del curso"></a>
            </div>
            <div class="course-info">
              <div class="course-text">
                <h5><a href="#">Nombre del curso</a></h5>
                <p>Lorem ipsum dolor sit amet, consectetur</p>
                <div class="students"><a href="#">Categor�a</a></div>
              </div>
              <div class="course-author">
                <div class="ca-pic set-bg" data-setbg="/assets/img/logos/logo-udemy.svg"><a href="#" title="Udemy"></a></div>
                <p><a href="#">Udemy</a></p>
              </div>
            </div>
          </div>
        </div>
				<!-- course -->
				<div class="course-item">
          <div class="course-item">
            <div class="course-thumb set-bg" data-setbg="/assets/img/courses/1.jpg">
              <div class="price"><a href="#">Avanzado</a></div>
              <a href="#" title="Nombre del curso"></a>
            </div>
            <div class="course-info">
              <div class="course-text">
                <h5><a href="#">Nombre del curso</a></h5>
                <p>Lorem ipsum dolor sit amet, consectetur</p>
                <div class="students"><a href="#">Categor�a</a></div>
              </div>
              <div class="course-author">
                <div class="ca-pic set-bg" data-setbg="/assets/img/logos/logo-udemy.svg"><a href="#" title="Udemy"></a></div>
                <p><a href="#">Udemy</a></p>
              </div>
            </div>
          </div>
        </div>
				<!-- course -->
				<div class="course-item">
          <div class="course-item">
            <div class="course-thumb set-bg" data-setbg="/assets/img/courses/1.jpg">
              <div class="price"><a href="#">Avanzado</a></div>
              <a href="#" title="Nombre del curso"></a>
            </div>
            <div class="course-info">
              <div class="course-text">
                <h5><a href="#">Nombre del curso</a></h5>
                <p>Lorem ipsum dolor sit amet, consectetur</p>
                <div class="students"><a href="#">Categor�a</a></div>
              </div>
              <div class="course-author">
                <div class="ca-pic set-bg" data-setbg="/assets/img/logos/logo-udemy.svg"><a href="#" title="Udemy"></a></div>
                <p><a href="#">Udemy</a></p>
              </div>
            </div>
          </div>
        </div>
				<!-- course -->
			</div>
		</div>
	</section>
	<!-- Page end -->

  <!-- footer section -->
  <footer class="footer-section spad pb-0">
    <div class="footer-top">
      <div class="footer-warp">
        <div class="row">
          <div class="widget-item">
            <h4>Contacto</h4>
            <ul class="contact-list">
              <li>Direcci�n</li>
              <li>Tel�fono</li>
              <li>email@email.com</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="footer-warp">
        <ul class="footer-menu">
          <li><a href="${ privacyUrl }">Pol�tica de privacidad</a></li>
        </ul>
        <div class="copyright">
          Copyright &copy;
          <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Template:
          <a href="https://colorlib.com" target="_blank">Colorlib</a>
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>
      </div>
    </div>
  </footer>
  <!-- footer section end -->


	<!--====== Javascripts & Jquery ======-->
	<script src="/assets/js/jquery-3.2.1.min.js"></script>
	<script src="/assets/js/bootstrap.bundle.min.js"></script>  
	<script src="/assets/js/mixitup.min.js"></script>
	<script src="/assets/js/circle-progress.min.js"></script>
	<script src="/assets/js/owl.carousel.min.js"></script>
	<script src="/assets/js/js.cookie.min.js"></script>
	<script src="/assets/js/all.js"></script>
	<script src="/assets/js/main.js"></script>

</html>
    