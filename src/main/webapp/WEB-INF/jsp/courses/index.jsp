<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 <!-- Page info -->
<div class="page-info-section set-bg" data-setbg="/assets/img/libro.jpg">
	<div class="container">
		<div class="site-breadcrumb">
			<a href="${ rootUrl }">Inicio</a>
			<span>Listado de cursos</span>
		</div>
	</div>
</div>
<!-- Page info end -->

<!-- course section -->
<section class="course-section spad pb-0">
	<div class="course-warp">
		<div class="row course-items-area">
			<c:forEach items="${ courses }" var="course">
		        <spring:url value="/courses/{courseId}" var="courseUrl" htmlEscape="true">
		          <spring:param name="courseId" value="${ course.getCourseId() }" />
		        </spring:url>
		        <spring:url value="/platforms/{platformId}" var="platformUrl" htmlEscape="true">
		          <spring:param name="platformId" value="${ course.getPlatform().getPlatformId() }" />
		        </spring:url>
		        <spring:url value="/categories/{categoryId}" var="categoryUrl" htmlEscape="true">
		          <spring:param name="categoryId" value="${ course.getCategory().getCategoryId() }" />
		        </spring:url>
		        <spring:url value="/level/{level}" var="levelUrl" htmlEscape="true">
		          <spring:param name="level" value="${ course.getLevel() }" />
		        </spring:url>
				<!-- course -->
				<div class="mix col-lg-3 col-md-4 col-sm-6 finance">
					<div class="course-item">
						<div class="course-item">
							<div class="course-thumb set-bg" data-setbg="${ course.getImageUrl() }">
								<div class="price"><a href="${ levelUrl }"><c:out value="${ course.getLevel() }" /></a></div>
								<a href="${courseUrl}"></a>
							</div>
							<div class="course-info">
								<div class="course-text">
									<h5><a href="${ courseUrl }"><c:out value="${ course.getTitle() }" /></a></h5>
									<p><c:out value="${ course.getDescription() }" /></p>
									<div class="students"><a href="${ categoryUrl }"><c:out value="${ course.getCategory().getName() }" /></a></div>
								</div>
								<div class="course-author">
									<div class="ca-pic set-bg" data-setbg="${ course.getPlatform().getUrlImagePlatform() }"><a href="${ platformUrl }"></a></div>
									<p><a href="${ platformUrl }"><c:out value="${ course.getPlatform().getName() }" /></a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</section>
<!-- course section end -->

<tiles:insertAttribute name="searchComponent"></tiles:insertAttribute>
