<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
 <!-- Page info -->
<div class="page-info-section set-bg" data-setbg="/assets/img/libro.jpg">
	<div class="container">
		<div class="site-breadcrumb">
			<a href="${ rootUrl }">Inicio</a>
			<span><c:out value="${ course.getTitle() }" /></span>
		</div>
	</div>
</div>
<!-- Page info end -->

<!-- single course section -->
<section class="single-course spad pb-0">
	<div class="container">
		<div class="course-meta-area">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="course-note"><c:out value="${ course.getLevel() }" /></div>
					<h3><c:out value="${ course.getTitle() }" /></h3>
					<div class="course-metas">
						<div class="course-meta">
							<div class="course-author">
								<div class="ca-pic set-bg" data-setbg="${ course.getPlatform().getUrlImagePlatform() }"></div>
								<h6>Plataforma</h6>
								<p><c:out value="${ course.getPlatform().getName() }" /></p>
							</div>
						</div>
						<div class="course-meta">
							<div class="cm-info">
								<h6>Categor�a</h6>
								<p><c:out value="${ course.getCategory().getName() }" /></p>
							</div>
						</div>
						<div class="course-meta">
							<div class="cm-info">
								<h6>Duraci�n</h6>
								<p><c:out value="${ course.getDuration() }" /> horas</p>
							</div>
						</div>
						<div class="course-meta">
							<div class="cm-info">
								<h6>Opiniones</h6>
								<p><a href="#" class="to-reviews"><c:out value="${ course.getRemarks().size() }" /> opiniones</a> <span class="rating">
								<c:choose>
									<c:when test="${ courseScore == 0 }">
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>
									<c:when test="${ courseScore == 1 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>
									<c:when test="${ courseScore == 2 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>
									<c:when test="${ courseScore == 3 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>										
										<i class="fa fa-star is-fade"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>
									<c:when test="${ courseScore == 4 }">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star is-fade"></i>
									</c:when>									
									<c:otherwise>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
									</c:otherwise>
								</c:choose>										
								</span></p>
							</div>
						</div>
					</div>
					<a href="#" class="site-btn buy-btn">Ir al curso</a>
				</div>
			</div>
		</div>
		<img src="/assets/img/courses/single.jpg" alt="" class="course-preview">
		<tiles:insertAttribute name="remarksComponent"></tiles:insertAttribute>			
	</div>
</section>
 <!-- single course section end -->

 	<!-- Page -->
<section class="realated-courses spad">
	<div class="course-warp">
		<h2 class="rc-title">Cursos relacionados</h2>
		<div class="rc-slider owl-carousel">
			<c:forEach items="${ relatedCourses }" var="rc">
				<spring:url value="/courses/{courseId}" var="courseUrl" htmlEscape="true">
		          <spring:param name="courseId" value="${ rc.getCourseId() }" />
		        </spring:url>
		        <spring:url value="/platforms/{platformId}" var="platformUrl" htmlEscape="true">
		          <spring:param name="platformId" value="${ rc.getPlatform().getPlatformId() }" />
		        </spring:url>
		        <spring:url value="/categories/{categoryId}" var="categoryUrl" htmlEscape="true">
		          <spring:param name="categoryId" value="${ rc.getCategory().getCategoryId() }" />
		        </spring:url>
		        <spring:url value="/level/{level}" var="levelUrl" htmlEscape="true">
		          <spring:param name="level" value="${ rc.getLevel() }" />
		        </spring:url>
				<!-- course -->
		        <div class="course-item">
					<div class="course-thumb set-bg" data-setbg="${ rc.getImageUrl() }">
						<div class="price"><a href="${ levelUrl }"><c:out value="${ rc.getLevel() }" /></a></div>
						<a href="${courseUrl}"></a>
					</div>
					<div class="course-info">
						<div class="course-text">
							<h5><a href="${courseUrl}"><c:out value="${ rc.getTitle() }" /></a></h5>
							<p><c:out value="${ rc.getDescription() }" /></p>
							<div class="students"><a href="${categoryUrl}"><c:out value="${ rc.getCategory().getName() }" /></a></div>
						</div>
						<div class="course-author">
							<div class="ca-pic set-bg" data-setbg="${ rc.getPlatform().getUrlImagePlatform() }"><a href="${platformUrl}"></a></div>
							<p><a href="${platformUrl}"><c:out value="${ rc.getPlatform().getName() }" /></a></p>
						</div>
					</div>
		        </div>
	        </c:forEach>
		</div>
	</div>
</section>
<!-- Page end -->
