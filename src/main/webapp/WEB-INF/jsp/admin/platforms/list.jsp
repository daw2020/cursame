<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Lista de plataformas</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/admin">Panel administración</a></li>
            <li class="breadcrumb-item active">Plataformas</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
        	<div class="card">
        		<div class="card-body">
        			<spring:url value="/admin/platforms/create" var="createPlatformUrl" htmlEscape="true" />
        			<a href="${ createPlatformUrl }" class="btn btn-primary"><i class="fas fa-plus"></i>Crear plataforma</a>
        			<table class="table table-responsive">
			          	<thead>
			          		<tr>
			          			<th>ID</th>
			          			<th>Nombre</th>
			          			<th>URL</th>
			          			<th>Acciones</th>
			         		</tr>
			          	</thead>
			          	<tbody>
			          		<c:forEach items="${ platforms }" var="platform">
			          			<spring:url value="/admin/platforms/${ platform.getPlatformId() }" var="platformUrl" htmlEscape="true" />
			          			<tr>
			          				<td><c:out value="${ platform.getPlatformId() }" /></td>
			          				<td><c:out value="${ platform.getName() }" /></td>
			          				<td><c:out value="${ platform.getUrl() }" /></td>
			          				<td>
		          						<form:form method="delete" action="${ platformUrl }" cssClass="form-inline">
			          						<a href="${ platformUrl }">
				          						<i class="fas fa-edit"></i>
			          						</a>
		          							<button type="submit" class="btn btn-link">
		          								<i class="fas fa-trash"></i>
		          							</button>
	          							</form:form>
          							</td>
			       				</tr>
			   				</c:forEach>
			          	</tbody>
					</table>
        		</div>
        	</div>
          
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>