<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<spring:url value="/admin/paths" var="pathsUrl" htmlEscape="true" />
<spring:url value="/admin" var="adminUrl" htmlEscape="true" />
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><c:out value="${ path.getPathId() != null ? 'Editar' : 'Crear' }" /> camino</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="${ adminUrl }">Panel administración</a></li>
            <li class="breadcrumb-item"><a href="${ pathsUrl }">Caminos</a></li>
            <li class="breadcrumb-item active">Editar camino</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
        	<div class="card">
        		<div class="card-body">
        			<form:form modelAttribute="path" method="POST" action="${ pathsUrl }">
       				<form:input path="pathId" type="hidden" />
        				<div class="form-group">
							<label for="inputName">Nombre</label>
							<form:input path="name" type="text" class="form-control" id="inputName" placeholder="Nombre de la plataforma" required="required" />
						</div>
						<div class="form-group">
							<label for="inputDescription">Descripción</label>
							<form:input path="description" type="text" class="form-control" id="inputDescription" placeholder="Descripción" required="required" />
						</div>
						<div class="form-group">
							<label for="inputImageUrl">URL imagen del camino</label>
							<form:input path="urlImagePath" type="text" class="form-control" id="inputImageUrl" placeholder="https://example.com" required="required" />
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
        			</form:form>
        		</div>
        	</div>
          
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>