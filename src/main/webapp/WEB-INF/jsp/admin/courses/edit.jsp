<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<spring:url value="/admin/courses" var="coursesUrl" htmlEscape="true" />
<spring:url value="/admin" var="adminUrl" htmlEscape="true" />
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><c:out value="${ course.getCourseId() != null ? 'Editar' : 'Crear' }" /> curso</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="${ adminUrl }">Panel administraci�n</a></li>
            <li class="breadcrumb-item"><a href="${ coursesUrl }">Cursos</a></li>
            <li class="breadcrumb-item active">Editar curso</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
        	<div class="card">
        		<div class="card-body">
        			<form:form modelAttribute="course" method="POST" action="${ coursesUrl }">
       				<form:input path="courseId" type="hidden" />
        				<div class="form-group">
							<label for="inputName">T�tulo</label>
							<form:input path="title" type="text" class="form-control" id="inputName" placeholder="T�tulo del curso" required="required" />
						</div>
						<div class="form-group">
							<label for="inputDescription">Descripci�n</label>
							<form:input path="description" type="text" class="form-control" id="inputDescription" placeholder="Descripci�n" required="required" />
						</div>
						<div class="form-group">
							<label for="inputUrl">URL del curso</label>
							<form:input path="url" type="url" class="form-control" id="inputUrl" placeholder="https://example.com" required="required" />
						</div>
						<div class="form-group">
							<label for="inputImageUrl">URL imagen del curso</label>
							<form:input path="imageUrl" type="text" class="form-control" id="inputImageUrl" placeholder="https://example.com" required="required" />
						</div>
						<div class="form-check">
							<form:checkbox path="paymentRequired" class="form-check-input" id="inputPaymentRequired" />
							<label for="inputPaymentRequired" class="form-check-label">&iquest;Requiere pago?</label>
						</div>
						<div class="form-group">
							<label for="inputDuration">Duraci�n</label>
							<form:input path="duration" type="number" min="0" id="inputDuration" class="form-control" />
						</div>
						<div class="form-group">
							<label for="inputLevel">Nivel</label>
                            <form:select path="level" id="inputLevel" class="form-control form-select">
                                <form:option value="">Selecciona nivel</form:option>
                                <c:forEach items="${ levels }" var="level">
                                    <form:option value="${ level.name() }" title="${ level.name() }" />
                                </c:forEach>
                            </form:select>
						</div>
						<div class="form-group">
							<label for="inputPlatform">Plataforma</label>
                            <form:select path="platform" id="inputPlatform" class="form-control form-select">
                            	<form:option value="">Selecciona plataforma</form:option>
                            	<form:options items="${ platforms }" itemValue="platformId" itemLabel="name" />
                            </form:select>
						</div>
						<div class="form-group">
							<label for="inputCategory">Categor�a</label>
                            <form:select path="category" id="search--category" class="form-control form-select">
                            	<form:option value="">Selecciona categor�a</form:option>
                            	<form:options items="${ categories }" itemValue="categoryId" itemLabel="name" />
                            </form:select>
						</div>
						<div class="form-group">
							<label for="inputPaths">Caminos</label>
							<form:select items="${ paths }" path="paths" itemValue="pathId" itemLabel="name" class="form-control" id="inputPaths" multiple="true" />
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
        			</form:form>
        		</div>
        	</div>
          
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>