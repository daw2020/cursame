<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content=""><!-- ToDo: Rellenar la descripci�n a partir de los textos del copy -->

  <!-- Favicon -->
  <link href="/assets/img/logo.png" rel="shortcut icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/admin/AdminLTE.css">

  <title>C�rsame - Cambiar contrase�a</title>
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <div class="site-logo">
      <a href="#"><img src="/assets/img/logo.svg" alt="C�rsame"></a>
      <h1><a href="<spring:url value= '/'/>">C�rsame</a></h1>
    </div>
  </div>
  <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg font-weight-bold">Gracias por confiar en C�rsame</p>
        <p>En breve, recibir�s un correo con las instrucciones para reestablecer tu contrase�a. Para volver a la p�gina principal sigue el siguiente enlace.</p>
        <p class="mb-0 text-center">
          <a href="<spring:url value= '/'/>">Ir a C�rsame</a>
        </p>
      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->
</body>
</html>
