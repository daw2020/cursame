<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<!DOCTYPE html>
<!-- Page info -->
<div class="page-info-section set-bg" data-setbg="/assets/img/page-bg/4.jpg">
	<div class="container">
		<div class="site-breadcrumb">
			<a href="#">Inicio</a>
			<span>Mis datos</span>
		</div>
	</div>
</div>
<!-- Page info end -->
<!-- Page -->
<section class="profile-page spad pb-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="contact-form-warp">
					<div class="section-title text-white text-left">
						<h2>Mis datos</h2>
						<p>En esta secci�n podr�s modificar tus datos personales, as� como tu clave de acceso. Generamos un avatar a partir de tu correo conect�ndo con Gravatar, de forma autom�tica! </p>
					</div>
					<form:form modelAttribute="user" class="contact-form">
						<div class="form-group">														
							<form:input id="userId" path="userId" type="hidden" />
						</div>
						<div class="form-group">														
							<form:input id="roles" path="roles" type="hidden" />
						</div>
						<div class="form-group">
							<label for="nombre">Tu nombre:</label>							
							<form:input id="username" path="username" type="text" placeholder="${ userDetails.getUsername() }" />
						</div>
						<div class="form-group">
							<label for="email">Tu email:</label>							
							<form:input id="email" path="email" type="text" placeholder="${ email }" />
						</div>
						<div class="form-group">
							<label for="password">Tu nueva contrase�a:</label>
							<form:input id="password" path="password" type="password" placeholder="Tu nueva contrase�a" />					
						</div>
						<div class="form-group">
							<label for="pwd2">Confirma tu nueva contrase�a:</label>
							<input type="password" placeholder="Confirma tu nueva contrase�a" id="pwd2">
						</div>
						<div class="row">
							<div class="col-12 col-md-10">
								<label for="url-avatar">URL de tu avatar:</label>
								<input type="text" placeholder="${ url }" id="url-avatar">
							</div>
							<div class="col-12 col-md-2 text-center text-md-right">
								<img src="${ url }" class="avatar" alt="Tu avatar">
							</div>
						</div>						
						<input type="submit" id="btnEdit" class="site-btn" value ="Guardar datos"/>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Page end -->