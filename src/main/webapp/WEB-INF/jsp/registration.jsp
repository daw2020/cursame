<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content=""><!-- ToDo: Rellenar la descripci�n a partir de los textos del copy -->
		
		<!-- Favicon -->
		<link href="/assets/img/logo.png" rel="shortcut icon">
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Dosis:wght@700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
		
		<!-- Stylesheets -->
		<link rel="stylesheet" href="/assets/plugins/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/admin/AdminLTE.css">
		
		<title>C�rsame - Registro de usuario</title>
	</head>

	  <body class="hold-transition register-page">
	  <div class="register-box">
	    <div class="register-logo">
	      <div class="site-logo">
	        <a href="<spring:url value= '/'/>"><img src="/assets/img/logo.svg" alt="C�rsame"></a>
	        <h1><a href="<spring:url value= '/'/>">C�rsame</a></h1>	        
	      </div>
	    </div>
	
	    <div class="card">
	      <div class="card-body register-card-body">
	        <p class="login-box-msg">Nueva cuenta</p>
	
	        <form:form method="POST" modelAttribute="userForm" class="form-signin ">
		        <spring:bind path="username">
		          <div class="input-group mb-3">		          			          			          
		            <label for="Username" class="sr-only">Nombre usuario:</label>		            
		            <form:input type="text" path="username" class="form-control ${status.error ? 'is-invalid' : ''}" placeholder="Nombre usuario" id="username"></form:input>		            		            
		            <form:errors path="username" class="invalid-feedback order-last" element="div"></form:errors>
		            <div class="input-group-append">
		              <div class="input-group-text">
		                <span class="fas fa-user"></span>
		              </div>
		            </div>		            
		          </div>
	          	</spring:bind>
	          	<spring:bind path="email">
		          <div class="input-group mb-3">
		            <label for="email" class="sr-only">Email:</label>
		            <form:input type="email" path="email" class="form-control ${status.error ? 'is-invalid' : ''}" placeholder="Email" id="email"></form:input>		            
		            <form:errors path="email" class="invalid-feedback order-last" element="div"></form:errors>
		            <div class="input-group-append">
		              <div class="input-group-text">
		                <span class="fas fa-envelope"></span>
		              </div>
		            </div>
		          </div>
	          </spring:bind>
	          <spring:bind path="password">
		          <div class="input-group mb-3 ${status.error ? 'has-error' : ''}">
		            <label for="password" class="sr-only">Contrase�a:</label>
		            <form:input type="password" path="password" class="form-control ${status.error ? 'is-invalid' : ''}" placeholder="Contrase�a" id="password"></form:input>		            
		            <form:errors path="password" class="invalid-feedback order-last" element="div"></form:errors>
		            <div class="input-group-append">
		              <div class="input-group-text">
		                <span class="fas fa-lock"></span>
		              </div>
		            </div>
		          </div>
	          </spring:bind>
	          <spring:bind path="passwordConfirm">
		          <div class="input-group mb-3 ${status.error ? 'has-error' : ''}">
		            <label for="repassword" class="sr-only">Repite contrase�a:</label>
		            <form:input type="password" path="passwordConfirm" class="form-control ${status.error ? 'is-invalid' : ''}" placeholder="Repite contrase�a" id="passwordConfirm"></form:input>
		            <form:errors path="passwordConfirm" class="invalid-feedback order-last" element="div"></form:errors>		            
		            <div class="input-group-append">
		              <div class="input-group-text">
		                <span class="fas fa-lock"></span>
		              </div>
		            </div>
		          </div>
	          </spring:bind>
	          <div class="row subform">
	            <div class="col-8">
	              <div class="icheck-primary">
	                <input type="checkbox" id="agreeTerms" name="terms" value="agree">
	                <label for="agreeTerms">
	                  Acepto las <a href="#">condiciones</a>
	                </label>
	              </div>
	            </div>
	            <!-- /.col -->
	            <div class="col-4">
	              <button type="submit" class="btn btn-primary btn-block">Registrar</button>
	            </div>
	            <!-- /.col -->
	          </div>
	        </form:form>
	
	        <a href="<spring:url value= '/login'/>" class="text-center">Ya tengo una cuenta</a>
	      </div>
	      <!-- /.form-box -->
	    </div><!-- /.card -->
	  </div>
	  <!-- /.register-box -->
	
	</body>
</html>
