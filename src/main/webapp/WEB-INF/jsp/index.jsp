<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${ !isAuthenticated }">
  <!-- Hero section -->
  <section class="hero-section set-bg" data-setbg="/assets/img/pizarra.jpg">
    <div class="container">
      <div class="hero-text text-white">
        <h2>Aprende sin horarios</h2>
        <p>Buscamos los cursos que mejor se adapten a tu perfil, para que los puedas hacer cuando y donde quieras.</p>
      </div>
      <div class="row">
        <div class="col-lg-2 offset-lg-5">
          <form class="intro-newslatter">
            <a class="site-btn" href="${ registrationUrl }">Reg�strate</a>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Hero section end -->
</c:if>

<!-- search section -->
<c:if test="${ isAuthenticated }">
  <div style="background-color:#4f6d7a">
  	<tiles:insertAttribute name="searchComponent"></tiles:insertAttribute>
  </div>
</c:if>
<!-- search section end -->

<!-- Paths section -->
<section class="categories-section spad" id="caminos">
  <div class="container">
    <div class="section-title">
      <h2>Nuestros caminos</h2>
      <p>Organizamos los cursos para que se complementen y consigas la mejor formaci�n posible. Escoge el camino que m�s te guste.</p>
    </div>
    <div class="row">
      <c:forEach items="${ paths }" var="path">
        <spring:url value="/paths/{pathId}" var="pathUrl" htmlEscape="true">
          <spring:param name="pathId" value="${ path.getPathId() }" />
        </spring:url>
        <!-- Path -->
        <div class="col-lg-4 col-md-6">
          <div class="categorie-item">
            <div class="ci-thumb set-bg" data-setbg="${ path.getUrlImagePath() }"><a href="${ pathUrl }"></a></div>
            <div class="ci-text">
              <h5><a href="${ pathUrl }"><c:out value="${ path.getName() }" /></a></h5>
              <p><c:out value="${ path.getDescription() }" /></p>
              <span><c:out value="${ path.courses.size() }" /> cursos</span>
            </div>
          </div>
        </div>
      </c:forEach>
    </div>
  </div>
</section>
<!-- Paths section end -->


<!-- course section -->
<section class="course-section spad" id="cursos">
  <div class="container">
    <div class="section-title mb-0">
      <h2>Cursos m�s valorados</h2>
      <p>Te mostramos los cursos mejor valorados por la comunidad.</p>
    </div>
  </div>
  <div class="course-warp">
    <ul class="course-filter controls">
      <li class="control active" data-filter="all">Todas</li>
      <c:forEach items="${ categories }" var="category">
       <li class="control" data-filter=".category-${ category.getCategoryId() }"><c:out value="${ category.getName() }" /></li>
      </c:forEach>
    </ul>
    <div class="row course-items-area">
      <!-- course -->
      <c:forEach items="${ courses }" var="course">
        <spring:url value="/courses/{courseId}" var="courseUrl" htmlEscape="true">
          <spring:param name="courseId" value="${ course.getCourseId() }" />
        </spring:url>
        <spring:url value="/platforms/{platformId}" var="platformUrl" htmlEscape="true">
          <spring:param name="platformId" value="${ course.getPlatform().getPlatformId() }" />
        </spring:url>
        <spring:url value="/categories/{categoryId}" var="categoryUrl" htmlEscape="true">
          <spring:param name="categoryId" value="${ course.getCategory().getCategoryId() }" />
        </spring:url>
        <spring:url value="/level/{level}" var="levelUrl" htmlEscape="true">
          <spring:param name="level" value="${ course.getLevel() }" />
        </spring:url>
     <div class="mix col-lg-3 col-md-4 col-sm-6 category-${ course.getCategory().getCategoryId() }">
          <div class="course-item">
            <div class="course-thumb set-bg" data-setbg="${ course.getImageUrl() }">
              <div class="price"><a href="${ levelUrl }"><c:out value="${ course.getLevel() }" /></a></div>
              <a href="${ courseUrl }"></a>
            </div>
            <div class="course-info">
              <div class="course-text">
                <h5><a href="${ courseUrl }"><c:out value="${ course.getTitle() }" /></a></h5>
               <p><c:out value="${ course.getDescription() }" /></p>
               <div class="students"><a href="${ categoryUrl }"><c:out value="${ course.getCategory().getName() }" /></a></div>
              </div>
              <c:if test="${ isAuthenticated }">
             <div class="course-author">
                  <div class="ca-pic set-bg" data-setbg="${ course.getPlatform().getUrlImagePlatform() }"><a href="${ platformUrl }"></a></div>
                  <p><a href="${ platformUrl }"><c:out value="${ course.getPlatform().getName() }" /></a></p>
                </div>
              </c:if>
            </div>
          </div>
        </div>
      </c:forEach>
    </div>
  </div>
</section>
<!-- course section end -->

<!-- signup section -->
<section class="signup-section spad" id="contacto">
  <div class="signup-bg set-bg" data-setbg="/assets/img/contacto.jpg"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6">
        <div class="signup-warp">
          <div class="section-title text-white text-left">
            <h2>Contacta con nosotros</h2>
            <p>�Tienes alguna duda? Rellena el formulario y te contestaremos lo antes posible.</p>
          </div>
          <!-- signup form -->
          <form class="signup-form">
            <label for="contacto-nombre" class="sr-only">Nombre:</label>
            <input type="text" placeholder="Tu nombre" id="contacto-nombre">
            <label for="contacto-email" class="sr-only">E-mail:</label>
            <input type="text" placeholder="Tu e-mail" id="contacto-email">
            <label for="contacto-consulta" class="sr-only">Consulta:</label>
            <textarea placeholder="Tu consulta" id="contacto-consulta"></textarea>
            <button class="site-btn">Enviar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- signup section end -->

<!-- banner section -->
<c:if test="${ !isAuthenticated }">
 <section class="banner-section spad" id="registro">
   <div class="container">
     <div class="section-title mb-0 pb-2">
       <h2>Reg�strate ahora</h2>
       <p>Obt�n acceso a los mejores itinerar�os de formaci�n registrandote. Tan s�lo te llevar� 2 minutos. �A qu� esperas a registrarte?</p>
     </div>
     <div class="text-center pt-5">	        
       <a class="site-btn" href="${ registrationUrl }">Reg�strate</a>
     </div>
   </div>
 </section>
</c:if>
