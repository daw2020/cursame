<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:url value="/" var="rootUrl" htmlEscape="true" />
<spring:url value="/userProfile" var="profile" htmlEscape="true" />
<spring:url value="/privacy" var="privacyUrl" htmlEscape="true" />
<!DOCTYPE html>
<html lang="es-ES">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content=""><!-- ToDo: Rellenar la descripci�n a partir de los textos del copy -->

  <!-- Favicon -->
  <link href="/assets/img/logo.png" rel="shortcut icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Dosis:wght@700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400&display=swap" rel="stylesheet">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/assets/css/font-awesome.min.css" />
  <link rel="stylesheet" href="/assets/css/owl.carousel.min.css" />
  <link rel="stylesheet" href="/assets/css/style.css" />

  <title>C�rsame<c:if test="${ pageTitle != null && pageTitle.length() > 0  }"> - <c:out value="${ pageTitle }" /></c:if></title>
</head>

<body>
  <spring:url value="/registration" var="registrationUrl" htmlEscape="true" />
  <spring:url value="/login" var="loginUrl" htmlEscape="true" />
  <!-- Page Preloder -->
  <div id="preloder">
    <div class="loader"></div>
  </div>

  <!-- Header section -->
  <header class="header-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3">
          <div class="site-logo">
            <a href="${ rootUrl }"><img src="/assets/img/logo.svg" class="logo" alt="C�rsame"></a>
            <h1><a href="${ rootUrl }">C�rsame</a></h1>
          </div>
          <div class="nav-switch">
            <i class="fa fa-bars"></i>
          </div>
        </div>
        <div class="col-lg-9 col-md-9">
          <c:choose>
            <c:when test="${ isAuthenticated }">
	      	  <a class="nav-link dropdown-toggle site-btn header-btn" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	            <span class="mr-2 d-none d-lg-inline text-gray-600">${ userDetails.getUsername() }</span>
	            <!-- <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"> -->
	          </a>
	          <!-- Dropdown - User Information -->
	          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" style="min-width: 196px;" aria-labelledby="userDropdown">
	            <a class="dropdown-item" href="${ profile }">
	              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
	              Perfil
	            </a>	             
	            <div class="dropdown-divider"></div>
	            <a onclick="document.forms['logoutForm'].submit()" class="dropdown-item">
	              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
	              Cerrar sesi�n
	            </a>
               	<form id="logoutForm" method="POST" action="${contextPath}/logout">
            		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        		</form>
	          </div>
        	</c:when>
        	<c:otherwise>
        		<a href="${ loginUrl }" class="site-btn header-btn">Acceder</a>        		
        	</c:otherwise>        	       
        </c:choose>        
         <nav class="main-menu">
           <ul>
             <li><a href="${ rootUrl }#buscador">Buscador</a></li>
             <li><a href="${ rootUrl }#caminos">Caminos</a></li>
             <li><a href="${ rootUrl }#cursos">Cursos</a></li>
             <li><a href="${ rootUrl }#contacto">Contacto</a></li>
             <c:if test="${ !isAuthenticated }">
             	<li><a href="${ rootUrl }#registro">Registro</a></li>
             </c:if>
           </ul>
         </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- Header section end -->

  <tiles:insertAttribute name="content"></tiles:insertAttribute>

  <!-- footer section -->
  <footer class="footer-section spad pb-0">
    <div class="footer-top">
      <div class="footer-warp">
        <div class="row">
          <div class="widget-item">
            <h4>Contacto</h4>
            <ul class="contact-list">
              <li>Direcci�n</li>
              <li>Tel�fono</li>
              <li>email@email.com</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="footer-warp">
        <ul class="footer-menu">
          <li><a href="${ privacyUrl }">Pol�tica de privacidad</a></li>
        </ul>
        <div class="copyright">
          Copyright &copy;
          <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Template:
          <a href="https://colorlib.com" target="_blank">Colorlib</a>
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>
      </div>
    </div>
  </footer>
  <!-- footer section end -->
  <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="cookie-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="cookie-title">Pol�tica cookies</h4>
        </div>
        <div class="modal-body">
        En esta web se utilizan cookies de terceros y propias para conseguir que tengas una mejor experiencia de navegaci�n, puedas compartir contenido en redes sociales y para que podamos obtener estad�sticas de los usuarios.
		Puedes evitar la descarga de cookies a trav�s de la configuraci�n de tu navegador, evitando que las cookies se almacenen en su dispositivo.
		Como propietario de este sitio web, te comunico que no utilizamos ninguna informaci�n personal procedente de cookies, tan s�lo realizamos estad�sticas generales de visitas que no suponen ninguna informaci�n personal.
		Es muy importante que leas la presente pol�tica de cookies y comprendas que, si contin�as navegando, consideraremos que aceptas su uso.
		Seg�n los t�rminos incluidos en el art�culo 22.2 de la Ley 34/2002 de Servicios de la Sociedad de la Informaci�n y Comercio Electr�nico, si contin�as navegando, estar�s prestando tu consentimiento para el empleo de los referidos mecanismos.
        </div>
        <div class="modal-footer">
          <a href="#a" class="btn btn-success btn-sm" data-dismiss="modal">Aceptar</a>
          <a href="${ privacyUrl }" class="btn btn-secondary btn-sm" data-dismiss="modal">M�s detalles</a>
        </div>
      </div>
    </div>
  </div>

  <!--====== Javascripts & Jquery ======-->
  <script src="/assets/js/jquery-3.2.1.min.js"></script>
  <script src="/assets/js/bootstrap.bundle.min.js"></script>  
  <script src="/assets/js/mixitup.min.js"></script>
  <script src="/assets/js/circle-progress.min.js"></script>
  <script src="/assets/js/owl.carousel.min.js"></script>
  <script src="/assets/js/js.cookie.min.js"></script>
  <script src="/assets/js/all.js"></script>
  <script src="/assets/js/main.js"></script>

</html>
