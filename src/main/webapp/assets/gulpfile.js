// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const replace = require('gulp-replace');
const del = require('del');
const rename = require('gulp-rename');

// File paths
const files = {
  scssPath: 'scss/**/*.scss',
  jsPath: 'js/**/*.js',
  cssPath: 'css'
}

function cleanTask() {
  return del([files.cssPath]);
}

function copyTask() {
  return src([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/font-awesome/css/font-awesome.min.css',
    'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
  ], { base: './' })
    .pipe(rename({ dirname: '' }))
    .pipe(dest('css'))
}

function copyFontsTask() {
  return src(['node_modules/font-awesome/fonts/**/*'], { base: './' })
    .pipe(rename({ dirname: '' }))
    .pipe(dest('fonts'))
}


// Sass task: compiles the style.scss file into style.css
function scssTask() {
  return src(files.scssPath)
    .pipe(sourcemaps.init()) // initialize sourcemaps first
    .pipe(sass()) // compile SCSS to CSS
    .pipe(postcss([autoprefixer(), cssnano()])) // PostCSS plugins
    .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
    .pipe(dest(files.cssPath)
    ); // put final CSS in dist folder
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask() {
  return src([
    files.jsPath
    //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
  ])
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(dest('dist')
    );
}

// Cachebust
function cacheBustTask() {
  const cbString = new Date().getTime();
  return src(['index.html'])
    .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
    .pipe(dest('.'));
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask() {
  watch([files.scssPath, files.jsPath],
    { interval: 1000, usePolling: true }, //Makes docker work
    series(
      //parallel(scssTask, jsTask),
      scssTask
      //cacheBustTask
    )
  );
}

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
  //parallel(scssTask, jsTask),
  cleanTask,
  scssTask,
  copyTask,
  copyFontsTask,
  //cacheBustTask,
  watchTask
);
