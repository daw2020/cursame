/* =================================
------------------------------------
	WebUni - Education Template
	Version: 1.0
 ------------------------------------
 ====================================*/


'use strict';

function showPreloader() {
  return setTimeout(() => $("#preloder").fadeIn(), 400);
}
function hidePreloader(timer) {
  if (timer) {
    clearTimeout(timer);
  }
  $("#preloder").fadeOut("slow");
}

let timerG = showPreloader();
$(window).on('load', function () {
	/*------------------
		Preloder
	--------------------*/
  hidePreloader(timerG);

	/*------------------
		Gallery item
	--------------------*/
  if ($('.course-items-area').length > 0) {
    var containerEl = document.querySelector('.course-items-area');
    var mixer = mixitup(containerEl);
  }

});

(function ($) {

	/*------------------
		Navigation
	--------------------*/
  $('.nav-switch').on('click', function (event) {
    $('.main-menu').slideToggle(400);
    event.preventDefault();
  });


	/*------------------
		Background Set
	--------------------*/
  function renderBg($elem) {
    var bg = $elem.data('setbg');
    $elem.css('background-image', 'url(' + bg + ')');
  }
  $('.set-bg').each(function () {
    renderBg($(this));
  });


	/*------------------
		Realated courses
	--------------------*/
  $('.rc-slider').owlCarousel({
    autoplay: true,
    loop: true,
    nav: true,
    dots: false,
    margin: 30,
    navText: ['', '<i class="fa fa-angle-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      576: {
        items: 2
      },
      990: {
        items: 3
      },
      1200: {
        items: 4
      }
    }
  });


  /*------------------
  Accordions
--------------------*/
  $('.panel-link').on('click', function (e) {
    $('.panel-link').removeClass('active');
    var $this = $(this);
    if (!$this.hasClass('active')) {
      $this.addClass('active');
    }
    e.preventDefault();
  });



	/*------------------
		Circle progress
	--------------------*/
  $('.circle-progress').each(function () {
    var cpvalue = $(this).data("cpvalue");
    var cpcolor = $(this).data("cpcolor");
    var cptitle = $(this).data("cptitle");
    var cpid = $(this).data("cpid");

    $(this).append('<div class="' + cpid + '"></div><div class="progress-info"><h2>' + cpvalue + '%</h2><p>' + cptitle + '</p></div>');

    if (cpvalue < 100) {

      $('.' + cpid).circleProgress({
        value: '0.' + cpvalue,
        size: 176,
        thickness: 9,
        fill: cpcolor,
        emptyFill: "rgba(0, 0, 0, 0)"
      });
    } else {
      $('.' + cpid).circleProgress({
        value: 1,
        size: 176,
        thickness: 9,
        fill: cpcolor,
        emptyFill: "rgba(0, 0, 0, 0)"
      });
    }

  });

  $('.main-menu a').each(function () {
    $(this).on('click', function () {
      const targetId = $(this).attr('href').slice(1);
      $("html, body").animate({ scrollTop: $('#' + targetId).offset().top }, 1000);
    })
  });

  $('.to-reviews').on('click', function(e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: $('#opiniones').offset().top }, 1000);
  });

  $('.toggle-busqueda').each(function () {
    $(this).on('click', function (e) {
      e.preventDefault();
      if ($(this).hasClass('toggle-busqueda-avanzada')) {
        $(this).addClass('d-none');
        $('.toggle-busqueda-simple').removeClass('d-none');
        $('.submit-busqueda-avanzada').addClass('d-none');
        $('.submit-busqueda-simple').removeClass('d-none');
        $('.campos-avanzado').removeClass('d-none');
      } else {
        $(this).addClass('d-none');
        $('.toggle-busqueda-avanzada').removeClass('d-none');
        $('.submit-busqueda-avanzada').removeClass('d-none');
        $('.submit-busqueda-simple').addClass('d-none');
        $('.campos-avanzado').addClass('d-none');
      }
    })
  })

  const processReviews = () => {
    const emptyReview = document.querySelector('.course-review-empty');
    const reviewsList = document.querySelector('.course-reviews-list');
    const renderReview = review => {
      const newReview = emptyReview.cloneNode(true);
      newReview.classList.remove('course-review-empty');
      const avatar = newReview.querySelector('.ca-pic');
      const name = newReview.querySelector('.review-user');
      const text = newReview.querySelector('.review-text');
      const rating = newReview.querySelector('.rating');

      name.textContent = review.name;
      avatar.dataset.setbg = `img/${review.avatar}`;
      renderBg($(avatar));
      text.textContent = review.text;
      for (let i = 1; i <= review.rating; i++) {
        rating.querySelector(`.fa-star:nth-child(${i})`).classList.remove('is-fade');
      }
      reviewsList.appendChild(newReview);
    }

    let pag = 0;
    const reviewsPerPag = 2;
    const urlReviews = 'revisiones.json';
    function loadReviews() {
      $.getJSON(`${urlReviews}?n=${reviewsPerPag}&pag=${++pag}`, data => {
        const reviews = data.reviews;
        for (const review of reviews) {
          renderReview(review);
        }
        if (pag >= data.pags) {
          $('.load-more').remove();
        }
        hidePreloader(timer);
      });
    }
	//loadReviews();
    let timer;
    $('.load-more').on('click', function(e) {
      e.preventDefault();
      timer = showPreloader();
      //loadReviews();
    });
  }  
  const fillStars = n => {
    $('.review-form .rating span svg').each(function(index, value){
		$('.review-form .rating span svg')[index].classList.add('is-fade');
	})
    for (let i = 0; i <= n-1; i++) {
      $(`.review-form .rating span svg`)[i].classList.remove('is-fade');
    }
    $('#score').val(n);
  }
  const unFillStars = () => {
    const rating = $('.review-form .rating span').data('score');
    for (let i = rating + 1; i <= 4; i++) {
      $(`.review-form .rating span svg`)[i].classList.add('is-fade');	  
    }	
  }
  const fixRating = i => {
    $('.review-form .rating span').data('score', i);
    fillStars(i);
    $('#score').val(i);
  }
  const starsRollover = () => {
    $('.review-form .rating span').on('mouseover', function() {
      fillStars($(this).data('score'));
    }).on('mouseout', function() {
      //unFillStars();
    }).on('click', function() {
      //fixRating($(this).data('valoracion'));
    })
  }

  if (document.querySelector(".course-reviews")) {
    processReviews();
    starsRollover();
    $('.add-review').on('click', function() {
      $(this).remove();
      $('.review-form').show();
    })
  }

  if (document.querySelector("#deleteModal")) {
    $('#deleteModal').on('show.bs.modal', function (event) {
      let button = $(event.relatedTarget);
      let hrefReview = button.data('href');
      let modal = $(this);
      modal.find('#deleteComment').data('href', hrefReview);
    });
    $("#deleteComment").on('click', function() {
      document.location = $(this).data('href');
    });
  }
  
  /*------------------
	Aviso cookies
  --------------------*/
  if(Cookies.get('avisoCookies') === undefined) {
    $('#cookieModal').modal('show');
    $('#cookieModal').on('hide.bs.modal', function() {
      Cookies.set('avisoCookies', '1', {expires: 365});
    });
  }
  
})(jQuery);

