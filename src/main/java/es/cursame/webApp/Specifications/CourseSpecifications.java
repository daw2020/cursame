package es.cursame.webApp.Specifications;

import java.util.Set;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Domain.Course;
import es.cursame.webApp.Domain.Course_;
import es.cursame.webApp.Domain.Level;
import es.cursame.webApp.Domain.Path;
import es.cursame.webApp.Domain.Path_;
import es.cursame.webApp.Domain.Platform;

public interface CourseSpecifications {
	public static Specification<Course> isLevel(Level level) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Course_.level), level);
	}
	
	public static Specification<Course> isCategory(Category category) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Course_.category), category);
	}
	
	public static Specification<Course> isPlatform(Platform platform) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Course_.platform), platform);
	}
	
	public static Specification<Course> containsName(String name) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Course_.title), "%" + name + "%");
	}
	
	public static Specification<Course> containsPath(String pathName) {
		
		return (root, cq, cb) -> {
			cq.distinct(true);
			Root<Course> course = root;
			Subquery<Path> pathSubQuery = cq.subquery(Path.class);
			Root<Path> path = pathSubQuery.from(Path.class);
			Expression<Set<Course>> coursePaths = path.get(Path_.courses);
			pathSubQuery.select(path);
			pathSubQuery.where(cb.like(path.get(Path_.name), "%" + pathName + "%"), cb.isMember(course, coursePaths));
			return cb.exists(pathSubQuery);
		};
	}
}
