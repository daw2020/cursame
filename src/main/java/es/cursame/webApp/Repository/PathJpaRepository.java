package es.cursame.webApp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cursame.webApp.Domain.*;

@Repository
public interface PathJpaRepository extends JpaRepository<Path, Long> {
	Path findFirstByNameIs(String name);
	List<Path> findByNameContaining(String name);
}
