package es.cursame.webApp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cursame.webApp.Domain.*;

@Repository
public interface RemarkJpaRepository extends JpaRepository<Remark, Long> {	
	List<Remark> findByScoreEquals(String name);
	List<Remark> findByScoreEquals(int score);
	List<Remark> findByScoreGreaterThan(int score);
	List<Remark> findByScoreGreaterThanEqual(int score);
	List<Remark> findByScoreLessThan(int score);
	List<Remark> findByScoreLessThanEqual(int score);
}
