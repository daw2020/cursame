package es.cursame.webApp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cursame.webApp.Domain.*;

@Repository
public interface RoleJpaRepository extends JpaRepository<Role, Long> {
	Role findFirstByRoleNameIs(String name);
}
