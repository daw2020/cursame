package es.cursame.webApp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;

import es.cursame.webApp.Domain.*;

@Repository
public interface CourseJpaRepository extends JpaRepository<Course, Long>, JpaSpecificationExecutor<Course> {
	List<Course> findByTitleIgnoreCaseStartingWith(String title);
	List<Course> findByTitleContaining(String title);	
	List<Course> findByPaymentRequiredTrue();
	List<Course> findByPaymentRequiredFalse();
	List<Course> findByDurationEquals(float duration);
	List<Course> findByDurationGreaterThan(float duration);
	List<Course> findByDurationGreaterThanEqual(float duration);
	List<Course> findByDurationLessThan(float duration);
	List<Course> findByDurationLessThanEqual(float duration);
	List<Course> findByLevelEquals(Level level);
}
