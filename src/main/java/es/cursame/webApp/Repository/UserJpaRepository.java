package es.cursame.webApp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cursame.webApp.Domain.*;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
	User findFirstByUsernameIs(String username);
	User findFirstByEmailIs(String email);	
}
