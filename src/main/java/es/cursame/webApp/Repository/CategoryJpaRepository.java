package es.cursame.webApp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cursame.webApp.Domain.*;

@Repository
public interface CategoryJpaRepository extends JpaRepository<Category, Long> {
	Category findFirstByNameIs(String name);
}
