package es.cursame.webApp.Service;

import java.util.List;

import es.cursame.webApp.Domain.Path;

public interface PathService {

	Path getPathByPathId(Long pathId);
	
	Path getPathByName(String name);
	
	List<Path> getPathsWhereNameContains(String name);
	
	List<Path> getAllPaths();
	
	void addPath(Path path);
	
	void updatePath(Path path);
	
	long getCount();
	
	void deletePath(Path path);
		
}
