package es.cursame.webApp.Service;

import es.cursame.webApp.Domain.User;

public interface UserService {

	User getUserByUserId(Long userId);
	
	User getUserByUsername(String username);
	
	User getUserByEmail(String email);	
	
	void addUser(User user);
	
	void updateUser(User user);
	
	void createPasswordResetTokenForUser(User user, String token);

	void changeUserPassword(User user, String password);
		
}
