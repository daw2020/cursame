package es.cursame.webApp.Service;

import java.util.List;

import es.cursame.webApp.DataTransferObjects.SearchForm;
import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Domain.Course;
import es.cursame.webApp.Domain.Level;

public interface CourseService {

	Course getCourseByCourseId(Long courseId);
		
	List<Course> getCoursesWherePaymentRequiredTrue();
	
	List<Course> getCoursesWherePaymentRequiredFalse();
	
	List<Course> getCoursesWhereDurationEquals(float duration);
	
	List<Course> getCoursesWhereLevelEquals(Level level);
	
	List<Course> getAllCourses();
	
	List<Course> searchCourses(SearchForm searchForm);
	
	void addCourse(Course course);
	
	void updateCourse(Course course);	
	
	long getCount();
	
	void deleteCourse(Course course);
}
