package es.cursame.webApp.Service;

import java.util.List;

import es.cursame.webApp.Domain.Platform;


public interface PlatformService {

	Platform getPlatformByPlatformId(Long platformId);
	
	Platform getPlatformByName(String name);
	
	List<Platform> getPlatformWhereNameContains(String name);	
	
	List<Platform> getAll();
		
	void addPlatform(Platform platform);
	
	void updatePlatform(Platform platform);
	
	long getCount();
	
	void deletePlatform(Platform platform);
		
}
