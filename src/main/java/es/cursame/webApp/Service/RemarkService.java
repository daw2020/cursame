package es.cursame.webApp.Service;

import java.util.List;

import es.cursame.webApp.Domain.Remark;

public interface RemarkService {

	Remark getRemarkByRemarkId(Long remarkId);		
	
	List<Remark> getRemarksWhereScoreEquals(int score);
	
	void addRemark(Remark remark);
	
	void updateRemark(Remark remark);
		
}
