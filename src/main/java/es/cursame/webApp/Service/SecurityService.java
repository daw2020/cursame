package es.cursame.webApp.Service;

public interface SecurityService {
    String findLoggedInUsername();
    
    String validatePasswordResetToken(long id, String token);

    void autoLogin(String username, String password);
}