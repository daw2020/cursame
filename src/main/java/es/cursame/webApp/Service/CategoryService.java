package es.cursame.webApp.Service;

import java.util.List;

import es.cursame.webApp.Domain.Category;

public interface CategoryService {

	Category getCategoryByCategoryId(Long categoryId);
	
	Category getCategoryByName(String name);
	
	List<Category> getAll();
	
	void addCategory(Category category);
	
	void updateCategory(Category category);
	
	long getCount();
	
	void deleteCategory(Category category);
		
}
