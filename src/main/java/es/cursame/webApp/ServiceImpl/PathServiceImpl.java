package es.cursame.webApp.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cursame.webApp.Domain.Path;
import es.cursame.webApp.Repository.PathJpaRepository;
import es.cursame.webApp.Service.PathService;


@Service
public class PathServiceImpl implements PathService{
		
	@Autowired
    private PathJpaRepository pathRepository;	

	@Override
	public Path getPathByPathId(Long pathId) {
		Optional<Path> optionalEntity =  pathRepository.findById(pathId);		
		return optionalEntity.get();
	}

	@Override
	public Path getPathByName(String name) {
		return pathRepository.findFirstByNameIs(name);
	}
	
	@Override
	public List<Path> getPathsWhereNameContains(String name) {
		return pathRepository.findByNameContaining(name);
	}
	
	@Override
	public List<Path> getAllPaths() {
		return pathRepository.findAll();
	}

	@Override
	public void addPath(Path path) {
		pathRepository.save(path);		
	}

	@Override
	public void updatePath(Path path) {
		pathRepository.save(path);		
	}

    @Override
    public long getCount() {
        return pathRepository.count();
    }
    
    @Override
    public void deletePath(Path path) {
        pathRepository.delete(path);
    }
	
}
