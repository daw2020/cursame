package es.cursame.webApp.ServiceImpl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import es.cursame.webApp.Domain.PasswordResetToken;
import es.cursame.webApp.Domain.Role;
import es.cursame.webApp.Domain.User;
import es.cursame.webApp.Repository.PasswordTokenJpaRepository;
import es.cursame.webApp.Repository.RoleJpaRepository;
import es.cursame.webApp.Repository.UserJpaRepository;
import es.cursame.webApp.Service.UserService;

@Service
public class UserServiceImpl implements UserService{
		
	@Autowired
    private UserJpaRepository userRepository;
	@Autowired
	private RoleJpaRepository roleRepository;
	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private PasswordTokenJpaRepository passwordTokenRepository;

	@Override
	public User getUserByUserId(Long userId) {
		Optional<User> optionalEntity =  userRepository.findById(userId);		
		return optionalEntity.get();
	}

	@Override
	public User getUserByUsername(String username) {
		return userRepository.findFirstByUsernameIs(username);
	}

	@Override
	public User getUserByEmail(String email) {
		return userRepository.findFirstByEmailIs(email);
	}

	@Override
	public void addUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));		
		List<Role> roles = Collections.singletonList( roleRepository.findFirstByRoleNameIs("ROLE_USER") );
		user.setRoles(roles);
		userRepository.save(user);	
	}

	@Override
	public void updateUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		List<Role> roles = Collections.singletonList( roleRepository.findFirstByRoleNameIs("ROLE_USER") );
		user.setRoles(roles);
		userRepository.save(user);		
	}
	
	public void createPasswordResetTokenForUser(User user, String token) {
	    PasswordResetToken myToken = new PasswordResetToken();
	    myToken.setUser(user);
	    myToken.setToken(token);
	    myToken.setExpiryDate(60);
	    passwordTokenRepository.save(myToken);
	}
	
	@Override
    public void changeUserPassword(User user, String password) {
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(user);
    }
	
}
