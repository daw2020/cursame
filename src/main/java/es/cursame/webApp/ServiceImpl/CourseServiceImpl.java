package es.cursame.webApp.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import es.cursame.webApp.DataTransferObjects.SearchForm;
import es.cursame.webApp.Domain.Course;
import es.cursame.webApp.Domain.Level;
import es.cursame.webApp.Repository.CourseJpaRepository;
import es.cursame.webApp.Service.CourseService;
import es.cursame.webApp.Specifications.CourseSpecifications;

@Service
public class CourseServiceImpl implements CourseService{
		
	@Autowired
    private CourseJpaRepository courseRepository;

	@Override
	public Course getCourseByCourseId(Long courseId) {
		Optional<Course> optionalEntity =  courseRepository.findById(courseId);		
		return optionalEntity.get();
	}

	@Override
	public List<Course> getCoursesWherePaymentRequiredTrue() { 
		return courseRepository.findByPaymentRequiredTrue();
	}

	@Override
	public List<Course> getCoursesWherePaymentRequiredFalse() {
		return courseRepository.findByPaymentRequiredFalse();
	}
	
	@Override
	public List<Course> getCoursesWhereDurationEquals(float duration) {
		return courseRepository.findByDurationEquals(duration);
	}

	@Override
	public List<Course> getCoursesWhereLevelEquals(Level level) {
		return courseRepository.findByLevelEquals(level);
	}

	@Override
	public void addCourse(Course course) {		 
		courseRepository.save(course);
	}

	@Override
	public void updateCourse(Course course) {
		courseRepository.save(course);		
	}

	@Override
	public List<Course> getAllCourses() {
		return courseRepository.findAll();
	}
	
	@Override
	public List<Course> searchCourses(SearchForm searchForm) {
		// Specification que sempre retorna true.
		Specification<Course> spec = (root, query, cb) -> cb.and();
		
		String name = searchForm.getName();
		if(name != null && name.length() > 0)
			spec = spec.and(CourseSpecifications.containsName(name));
		
		String path = searchForm.getPath();
		if(path != null && path.length() > 0)
			spec = spec.and(CourseSpecifications.containsPath(path));
		
		if(searchForm.getCategory() != null)
			spec = spec.and(CourseSpecifications.isCategory(searchForm.getCategory()));
		
		if(searchForm.getPlatform() != null)
			spec = spec.and(CourseSpecifications.isPlatform(searchForm.getPlatform()));
		
		if(searchForm.getLevel() != null)
			spec=spec.and(CourseSpecifications.isLevel(searchForm.getLevel()));
		
		return courseRepository.findAll(spec);
	}

    @Override
    public long getCount() {
        return courseRepository.count();
    }
    
    @Override
    public void deleteCourse(Course course) {
        courseRepository.delete(course);
    }

}
