package es.cursame.webApp.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cursame.webApp.Domain.Remark;
import es.cursame.webApp.Repository.RemarkJpaRepository;
import es.cursame.webApp.Service.RemarkService;

@Service
public class RemarkServiceImpl implements RemarkService{
		
	@Autowired
    private RemarkJpaRepository remarkRepository;

	@Override
	public Remark getRemarkByRemarkId(Long remarkId) {
		Optional<Remark> optionalEntity =  remarkRepository.findById(remarkId);		
		return optionalEntity.get();
	}
	
	@Override
	public List<Remark> getRemarksWhereScoreEquals(int score) {
		return remarkRepository.findByScoreEquals(score);
	}

	@Override
	public void addRemark(Remark remark) {
		remarkRepository.save(remark);		
	}

	@Override
	public void updateRemark(Remark remark) {
		remarkRepository.save(remark);		
	}
	
}
