package es.cursame.webApp.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Repository.PlatformJpaRepository;
import es.cursame.webApp.Service.PlatformService;

@Service
public class PlatformServiceImpl implements PlatformService{
		
	@Autowired
    private PlatformJpaRepository platformRepository;
	
	@Override
	public Platform getPlatformByPlatformId(Long platformId) {
		Optional<Platform> optionalEntity =  platformRepository.findById(platformId);		
		return optionalEntity.get();
	}

	@Override
	public Platform getPlatformByName(String name) {		
		return platformRepository.findFirstByNameIs(name);
	}
	
	@Override
	public List<Platform> getPlatformWhereNameContains(String name) {		
		return platformRepository.findByNameContaining(name);
	}
	
	@Override
	public List<Platform> getAll() {
		return platformRepository.findAll();
	}

	@Override
	public void addPlatform(Platform platform) {
		platformRepository.save(platform);		
	}

	@Override
	public void updatePlatform(Platform platform) {
		platformRepository.save(platform);		
	}

    @Override
    public long getCount() {
        return platformRepository.count();
    }
    
    @Override
    public void deletePlatform(Platform platform) {
        platformRepository.delete(platform);
    }
	
}
