package es.cursame.webApp.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Repository.CategoryJpaRepository;
import es.cursame.webApp.Service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService{
		
	@Autowired
    private CategoryJpaRepository categoryRepository;

	@Override
	public Category getCategoryByCategoryId(Long categoryId) {
		Optional<Category> optionalEntity =  categoryRepository.findById(categoryId);		
		return optionalEntity.get();
	}

	@Override
	public Category getCategoryByName(String name) {
		return categoryRepository.findFirstByNameIs(name);
	}

	@Override
	public void addCategory(Category category) {
		categoryRepository.save(category);		
	}

	@Override
	public void updateCategory(Category category) {
		categoryRepository.save(category);		
	}
	
	@Override
	public void deleteCategory(Category category) {
	    categoryRepository.delete(category);
	}

	@Override
	public List<Category> getAll() {
		return categoryRepository.findAll();
	}

    @Override
    public long getCount() {
        return categoryRepository.count();
    }	
	
}
