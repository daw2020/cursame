package es.cursame.webApp.Domain;

public enum Level {
	BEGINNER,
	INTERMEDIATE,
	ADVANCED
}
