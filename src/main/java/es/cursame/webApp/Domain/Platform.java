package es.cursame.webApp.Domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "platform")
public class Platform {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="platform_id")
	private Long platformId;
	
	private String name;
	private String url;
	private String urlImagePlatform;
	private String description;	
	
		
	@OneToMany(mappedBy="platform", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Course> courses;
	
	@OneToMany(mappedBy = "platform", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Remark> remarks;
	
	public Long getPlatformId() {
		return platformId;
	}
	
	public void setPlatformId(Long platformId) {
	    this.platformId = platformId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public List<Remark> getRemarks() {
		return remarks;
	}

	public void setRemarks(List<Remark> remarks) {
		this.remarks = remarks;
	}	

	public String getUrlImagePlatform() {
		return urlImagePlatform;
	}

	public void setUrlImagePlatform(String urlImagePlatform) {
		this.urlImagePlatform = urlImagePlatform;
	}

	public Platform(String name, String url, String urlImagePlatform, String description) {
		super();
		this.name = name;
		this.url = url;
		this.urlImagePlatform = urlImagePlatform;
		this.description = description;		
		this.courses = new ArrayList<Course>();
		this.remarks = new ArrayList<Remark>();
	}

	public Platform() {
		
	}
	
	public Platform(Long platformId) {
		super();
		this.platformId = platformId;
	}
	
	
}

