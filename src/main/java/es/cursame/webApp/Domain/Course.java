package es.cursame.webApp.Domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;

@Entity
@Table(name="course")
public class Course {
	@Id	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="course_id")
	private Long courseId;

	private String title;
	private String description;
	private String url;
	private String imageUrl;	
	private boolean paymentRequired;
	private float duration;		
	private Level level;	
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	private Category category;
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	private Platform platform;
	@OneToMany(mappedBy="course", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Remark> remarks;
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name="Path_has_Course", joinColumns=@JoinColumn(name="course_id"), inverseJoinColumns=@JoinColumn(name="path_id"))
	private Set<Path> paths = new HashSet<>();
	
	@Formula("(select coalesce(avg(r.score), 0) from remark r where r.course_course_id = course_id)")
	private float scoreAverage;
	
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public boolean isPaymentRequired() {
		return paymentRequired;
	}
	public void setPaymentRequired(boolean requiresPayment) {
		this.paymentRequired = requiresPayment;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}	
	public float getDuration() {
		return duration;
	}
	public void setDuration(float length) {
		this.duration = length;
	}		
	public List<Remark> getRemarks() {
		return remarks;
	}
	public void setRemarks(List<Remark> remarks) {
		this.remarks = remarks;
	}	
	public Set<Path> getPaths() {
		return paths;
	}
	public void setPaths(Set<Path> paths) {
		this.paths = paths;
	}		
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Platform getPlatform() {
		return platform;
	}
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}		
	
	public Course(String title, String description, String url, String imageUrl, boolean paymentRequired,
			float duration, Level level, Category category, Platform platform) {
		super();
		this.title = title;
		this.description = description;
		this.url = url;
		this.imageUrl = imageUrl;		
		this.paymentRequired = paymentRequired;
		this.duration = duration;
		this.level = level;
		this.category = category;
		this.platform = platform;
		this.remarks = new ArrayList<Remark>();
	}
	
	public Course() {}
	
	
}

