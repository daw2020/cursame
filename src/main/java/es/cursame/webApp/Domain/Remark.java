package es.cursame.webApp.Domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "remark")
public class Remark {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="remark_id")
	private Long remarkId;
			
	private String body;	
	private Integer score;
	@ManyToOne(fetch = FetchType.LAZY)
	private Platform platform;
	@ManyToOne(fetch = FetchType.LAZY)
	private Course course;
	@ManyToOne(fetch = FetchType.LAZY)
	private User user;
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}	
	public Platform getPlatform() {
		return platform;
	}
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}		
	
	public Remark(String body, Integer score, Course course, Platform platform, User user) {
		super();		
		this.body = body;
		this.score = score;		
		this.course = course;
		this.platform = platform;
		this.user = user;
	}
	
	public Remark() {
		super();
	}
	
}

