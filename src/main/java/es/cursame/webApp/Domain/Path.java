package es.cursame.webApp.Domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "path")
public class Path {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="path_id")
	private Long pathId;
	
	private String name;
	private String description;
	private String urlImagePath;
	
	@ManyToMany(mappedBy = "paths")
	private Set<Course> courses = new HashSet<Course>();
	
	public Long getPathId() {
		return pathId;
	}
	
	public void setPathId(Long pathId) {
	    this.pathId = pathId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public String getUrlImagePath() {
		return urlImagePath;
	}

	public void setUrlImagePath(String urlImagePath) {
		this.urlImagePath = urlImagePath;
	}

	public Path(String name, String description, String urlImagePath) {
		super();
		this.name = name;
		this.description = description;
		this.urlImagePath = urlImagePath;
	}
	
	public Path() {
		
	}
	
	
}

