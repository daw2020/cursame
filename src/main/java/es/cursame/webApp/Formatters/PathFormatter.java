package es.cursame.webApp.Formatters;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import es.cursame.webApp.Domain.Path;
import es.cursame.webApp.Service.PathService;

public class PathFormatter implements Formatter<Path> {
	@Autowired
	private PathService pathService;

	@Override
	public String print(Path object, Locale locale) {
		return object.getPathId().toString();
	}

	@Override
	public Path parse(String text, Locale locale) throws ParseException {
		return pathService.getPathByPathId(Long.parseLong(text));
	}

}
