package es.cursame.webApp.Formatters;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Service.CategoryService;

public class CategoryFormatter implements Formatter<Category> {
	@Autowired
	private CategoryService categoryService;

	@Override
	public String print(Category object, Locale locale) {
		return object.getCategoryId().toString();
	}

	@Override
	public Category parse(String text, Locale locale) throws ParseException {
		return categoryService.getCategoryByCategoryId(Long.parseLong(text));
	}

}
