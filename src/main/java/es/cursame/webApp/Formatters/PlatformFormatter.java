package es.cursame.webApp.Formatters;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Service.PlatformService;

public class PlatformFormatter implements Formatter<Platform> {
	@Autowired
	private PlatformService platformService;

	@Override
	public String print(Platform object, Locale locale) {
		return object.getPlatformId().toString();
	}

	@Override
	public Platform parse(String text, Locale locale) throws ParseException {
		return platformService.getPlatformByPlatformId(Long.parseLong(text));
	}

}
