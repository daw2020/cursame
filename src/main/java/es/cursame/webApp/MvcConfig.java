package es.cursame.webApp;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import es.cursame.webApp.Formatters.CategoryFormatter;
import es.cursame.webApp.Formatters.PlatformFormatter;

@Configuration
@ComponentScan(basePackages = "es.cursame.webApp")
public class MvcConfig implements WebMvcConfigurer {

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(new Locale("es", "Es"));
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }
    
    @Bean
    public CategoryFormatter categoryFormatter() {
    	CategoryFormatter cf = new CategoryFormatter();
    	return cf;
    }
    
    @Bean
    public PlatformFormatter platformFormatter() {
    	PlatformFormatter pf = new PlatformFormatter();
    	return pf;
    }
    
    @Override
    public void addFormatters(FormatterRegistry registry) {
    	registry.addFormatter(categoryFormatter());
    	registry.addFormatter(platformFormatter());
    }
    
    @Bean(name="messageSource")
	public ResourceBundleMessageSource bundleMessageSource() {
	    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	    messageSource.setBasename("validation");
	    return messageSource;
    }
}