package es.cursame.webApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import es.cursame.webApp.Domain.Level;
import es.cursame.webApp.Service.CourseService;

@Controller // This means that this class is a Controller
@RequestMapping("/levels")
public class LevelController implements MainTemplateController {
  @Autowired
  private CourseService courseService;
  
  @GetMapping("/{level}")
  public String getCourse(@PathVariable("level") Level level, Model model) {
	  
      
      model.addAttribute("courses", courseService.getCoursesWhereLevelEquals(level));
      model.addAttribute("pageTitle", level.name());
      return "courses.index";
  }
}