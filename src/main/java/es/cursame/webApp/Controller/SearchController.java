package es.cursame.webApp.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import es.cursame.webApp.DataTransferObjects.SearchForm;
import es.cursame.webApp.Service.CourseService;

@Controller
@RequestMapping("/search")
public class SearchController {
	@Autowired
	private CourseService courseService;
	
	@PostMapping
	public String search(Model model, @ModelAttribute("searchForm") SearchForm searchForm) {
		model.addAttribute("courses", courseService.searchCourses(searchForm));
		model.addAttribute("searchForm", searchForm);
		model.addAttribute("pageTitle", "Búsqueda");
		return "courses.index";
	}
}
