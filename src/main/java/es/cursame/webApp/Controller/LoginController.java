package es.cursame.webApp.Controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Locale;
import java.util.UUID;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import es.cursame.webApp.MD5Util;
import es.cursame.webApp.UserValidator;
import es.cursame.webApp.Domain.User;
import es.cursame.webApp.Service.SecurityService;
import es.cursame.webApp.Service.UserService;

@Controller
public class LoginController {
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;
    
    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    private MessageSource messages;
    
    @Autowired
    private Environment env;
    
	@Resource(name="authenticationManager")
    private AuthenticationManager authManager;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
    public String loginerror(Model model) {
        model.addAttribute("error", "true");
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void login(@RequestParam("username") String username, @RequestParam("password") String password, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authReq =
            new UsernamePasswordAuthenticationToken(username, password);
        Authentication auth = authManager.authenticate(authReq);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT", sc);
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)    
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)    
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.addUser(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/";
    }
    
    @RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)    
    public String forgotPassword(Model model) {        
        return "forgotPassword";
    }
        
    @RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
    public String resetPassword(HttpServletRequest request, @RequestParam("email") String userEmail){
        User user = userService.getUserByEmail(userEmail);
        if (user == null) {
        	//throw new UserNotFoundException();
        }
        String token = UUID.randomUUID().toString();
        userService.createPasswordResetTokenForUser(user, token);
        mailSender.send(buildResetTokenEmail(getAppUrl(request), 
          request.getLocale(), token, user));
        return "redirect:/thanks";
    }
    
    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
    
    private MimeMessage buildResetTokenEmail(String contextPath, Locale locale, String token, User user) {
        String url = contextPath + "/changePassword?id=" + user.getUserId() + "&token=" + token;
        String message = messages.getMessage("message.resetPassword", null, locale);
        File template;
        String body = message;
		try {
			template = resourceLoader.getResource("assets/templatePasswordReset.html").getFile();
			body = new String(
	        	      Files.readAllBytes(template.toPath()));
			body = body.replace("##recordarEnlace##", url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return buildEmail("Reset Password", body, user);
    }
    
    private MimeMessage buildEmail(String subject, String body, User user) {
    	MimeMessage mimeMessage = mailSender.createMimeMessage();
    	MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
    	String htmlMsg = body;    	
    	try {
			helper.setText(htmlMsg, true);
			helper.setTo(user.getEmail());
	    	helper.setSubject(subject);
	    	helper.setFrom(env.getProperty("support.email"));
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Use this or above line.
    	
    	return mimeMessage;
    }
    
    @GetMapping("/changePassword")
    public String showChangePasswordPage(Locale locale, Model model, @RequestParam("id") long id, @RequestParam("token") String token) {
        String result = securityService.validatePasswordResetToken(id, token);
        if (result != null) {
            model.addAttribute("message", 
              messages.getMessage("auth.message." + result, null, locale));
            return "redirect:/login?lang=" + locale.getLanguage();
        }
        return "/updatePassword";
    }
    
    @RequestMapping(value = "/updatePassword",	 method = RequestMethod.GET)
    public String updatePassword() {
        return "updatePassword";
    }
       
    @RequestMapping(value = "/savePassword", method = RequestMethod.POST)
    public String savePassword(@RequestParam("password") String password, @RequestParam("passwordConfirm") String passwordConfirm) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
	        	.getAuthentication().getPrincipal();
    	User user = userService.getUserByUsername(userDetails.getUsername());
    	userService.changeUserPassword(user, password);
    	return "redirect:/login";
    }
    
    @RequestMapping(value = "/thanks", method = RequestMethod.GET)    
    public String thanks() {        
        return "thanks";
    }
    
    @RequestMapping(value = "/userProfile", method = RequestMethod.GET)    
    public String userProfile(Model model) {
    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
	        	.getAuthentication().getPrincipal();
    	User user = userService.getUserByUsername(userDetails.getUsername());  
    	user.setPassword("");
    	String hash = MD5Util.md5Hex(user.getEmail());
    	String url = "https://www.gravatar.com/avatar/".concat(hash); 
    	model.addAttribute("user", user);
    	model.addAttribute("url", url);
    	model.addAttribute("email", user.getEmail());
        return "userProfile";
    }
    
    @RequestMapping(value = "/userProfile", method = RequestMethod.POST)
    public String updateUserDetails(@ModelAttribute("user") User newUser, BindingResult result) {
        String[] suppressedFields = result.getSuppressedFields();
        if (suppressedFields.length > 0) {
            throw new RuntimeException("Attempting to bind disallowed fields: " + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }
        userService.updateUser(newUser);
        return "redirect:/logout";
    }
   
}