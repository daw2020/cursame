package es.cursame.webApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Service.PlatformService;

@Controller // This means that this class is a Controller
@RequestMapping("/platforms")
public class PlatformController implements MainTemplateController {
  @Autowired
  private PlatformService platformService;
  
  @GetMapping("/{platformId}")
  public String getCourse(@PathVariable("platformId") Long code, Model model) {
	  
	  Platform platform = platformService.getPlatformByPlatformId(code);
      
      model.addAttribute("courses", platform.getCourses());
      model.addAttribute("pageTitle", platform.getName());
      return "courses.index";
  }
}