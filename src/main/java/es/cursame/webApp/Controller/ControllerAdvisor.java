package es.cursame.webApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import es.cursame.webApp.Domain.User;
import es.cursame.webApp.Service.UserService;

@ControllerAdvice
public class ControllerAdvisor {
  
    @Autowired
    private UserService userService;
	
	@ModelAttribute("isAuthenticated")
	@Transactional(readOnly = true)
	public boolean getIsAuthenticated() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth != null && !(auth instanceof AnonymousAuthenticationToken) && auth.isAuthenticated();
	}
	
	@ModelAttribute("userDetails")
	@Transactional(readOnly = true)
	public UserDetails getUserDetails() {
		return getIsAuthenticated() ? (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal() : null;
	}
	
	@ModelAttribute("userEntity")
	@Transactional(readOnly = true)
	public User getUserEntity() {
	    return getUserDetails() != null ? userService.getUserByUsername(getUserDetails().getUsername()) : null;
	}
}
