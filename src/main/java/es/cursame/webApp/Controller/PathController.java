package es.cursame.webApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import es.cursame.webApp.Domain.Path;
import es.cursame.webApp.Service.PathService;

@Controller // This means that this class is a Controller
@RequestMapping("/paths")
public class PathController implements MainTemplateController {
  @Autowired
  private PathService pathService;
  
  @GetMapping("/{pathId}")
  public String getCourse(@PathVariable("pathId") Long code, Model model) {
      
	  Path path = pathService.getPathByPathId(code);
	  
      
      model.addAttribute("courses", path.getCourses());
      model.addAttribute("pageTitle", path.getName());
      return "courses.index";
  }
}