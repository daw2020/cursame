package es.cursame.webApp.Controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import es.cursame.webApp.Domain.Path;
import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Service.PathService;

@Controller // This means that this class is a Controller
@RequestMapping("/admin/paths")
public class AdminPathController implements AdminTemplateController {
  @Autowired
  private PathService pathService;
  
  @GetMapping
  public ModelAndView getPaths() {
      ModelAndView mav = new ModelAndView("admin.paths.list");
      mav.addObject("paths", pathService.getAllPaths());
      mav.addObject("pageTitle", "Categorías");
      return mav;
  }
  
  @PostMapping
  public RedirectView savePath(@ModelAttribute("path") Path path) {
      Path savePath = new Path();
      if(path.getPathId() != null)
          savePath = pathService.getPathByPathId(path.getPathId());
      savePath.setName(path.getName());
      savePath.setDescription(path.getDescription());
      savePath.setUrlImagePath(path.getUrlImagePath());
      pathService.updatePath(savePath);
      return new RedirectView("/admin/paths");
  }
  
  @GetMapping("/create")
  public ModelAndView createPath() {
    
    ModelAndView mav = new ModelAndView("admin.paths.edit");
    mav.addObject("path", new Path());
    mav.addObject("pageTitle", "Crear camino");
    return mav;
  }
  
  @GetMapping("/{pathId}")
  public ModelAndView getPath(@PathVariable("pathId") Long code) {
      
	  Path path = pathService.getPathByPathId(code);
	  
      ModelAndView mav = new ModelAndView("admin.paths.edit");
      mav.addObject("path", path);
      mav.addObject("pageTitle", "Editar camino");
      return mav;
  }
  
  @DeleteMapping("/{platformId}")
  public RedirectView deleteCategory(@PathVariable("platformId") Long code) {
    
      Path path = pathService.getPathByPathId(code);
      
      pathService.deletePath(path);
      return new RedirectView("/admin/paths");
  }
}