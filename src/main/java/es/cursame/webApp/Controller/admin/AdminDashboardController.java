package es.cursame.webApp.Controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import es.cursame.webApp.Service.CategoryService;
import es.cursame.webApp.Service.CourseService;
import es.cursame.webApp.Service.PathService;
import es.cursame.webApp.Service.PlatformService;

@Controller
@RequestMapping("/admin")
public class AdminDashboardController {
  
  
  @Autowired
  private CategoryService categoryService;
  
  @Autowired
  private CourseService courseService;
  
  @Autowired
  private PathService pathService;
  
  @Autowired
  private PlatformService platformService;
  
  @GetMapping("/dashboard")
  public ModelAndView getDashboard() {
      
      
      ModelAndView mav = new ModelAndView("admin.index");
      
      
      //mav.addObject("courses", category.getCourses());
      //mav.addObject("pageTitle", category.getName());
      mav.addObject("categories", categoryService.getCount());
      mav.addObject("courses", courseService.getCount());
      mav.addObject("paths", pathService.getCount());
      mav.addObject("platforms", pathService.getCount());
      mav.addObject("pageTitle", "Panel Administración");
      return mav;
  }
  
  @GetMapping
  public RedirectView getIndex() {
      return new RedirectView("/admin/dashboard");
  }
}
