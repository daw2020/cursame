package es.cursame.webApp.Controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import es.cursame.webApp.Domain.Course;
import es.cursame.webApp.Domain.Level;
import es.cursame.webApp.Service.CategoryService;
import es.cursame.webApp.Service.CourseService;
import es.cursame.webApp.Service.PathService;
import es.cursame.webApp.Service.PlatformService;

@Controller // This means that this class is a Controller
@RequestMapping("/admin/courses")
public class AdminCourseController implements AdminTemplateController {
  @Autowired
  private CourseService courseService;
  @Autowired
  private PlatformService platformService;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  private PathService pathService;
  
  @GetMapping
  public ModelAndView getCourses() {
	  
      ModelAndView mav = new ModelAndView("admin.courses.list");
      
      
      mav.addObject("courses", courseService.getAllCourses());
      mav.addObject("pageTitle", "Cursos");
      return mav;
  }
  
  @PostMapping
  public RedirectView saveCourse(@ModelAttribute("course") Course course) {
      Course saveCourse = new Course();
      if(course.getCourseId() != null)
          saveCourse = courseService.getCourseByCourseId(course.getCourseId());
      saveCourse.setTitle(course.getTitle());
      saveCourse.setDescription(course.getDescription());
      saveCourse.setUrl(course.getUrl());
      saveCourse.setImageUrl(course.getImageUrl());
      saveCourse.setPaymentRequired(course.isPaymentRequired());
      saveCourse.setDuration(course.getDuration());
      saveCourse.setLevel(course.getLevel());
      saveCourse.setCategory(course.getCategory());
      saveCourse.setPlatform(course.getPlatform());
      saveCourse.setPaths(course.getPaths());
      courseService.updateCourse(saveCourse);
      return new RedirectView("/admin/courses");
  }
  
  @GetMapping("/create")
  public ModelAndView createCourse() {
    
    ModelAndView mav = new ModelAndView("admin.courses.edit");
    mav.addObject("platforms", platformService.getAll());
    mav.addObject("categories", categoryService.getAll());
    mav.addObject("paths", pathService.getAllPaths());
    mav.addObject("levels", Level.values());
    mav.addObject("course", new Course());
    mav.addObject("pageTitle", "Crear curso");
    return mav;
  }
  
  @GetMapping("/{courseId}")
  public ModelAndView getCourse(@PathVariable("courseId") Long code) {
      
      Course course = courseService.getCourseByCourseId(code);
      
      ModelAndView mav = new ModelAndView("admin.courses.edit");
      mav.addObject("platforms", platformService.getAll());
      mav.addObject("categories", categoryService.getAll());
      mav.addObject("paths", pathService.getAllPaths());
      mav.addObject("levels", Level.values());
      mav.addObject("course", course);
      mav.addObject("pageTitle", "Editar plataforma");
      return mav;
  }
  
  @DeleteMapping("/{courseId}")
  public RedirectView deleteCategory(@PathVariable("courseId") Long code) {
    
      Course course = courseService.getCourseByCourseId(code);
      
      courseService.deleteCourse(course);
      return new RedirectView("/admin/courses");
  }
}