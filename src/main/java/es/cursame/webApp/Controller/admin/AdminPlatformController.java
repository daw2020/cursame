package es.cursame.webApp.Controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Service.PlatformService;

@Controller // This means that this class is a Controller
@RequestMapping("/admin/platforms")
public class AdminPlatformController implements AdminTemplateController {
  @Autowired
  private PlatformService platformService;
  
  @GetMapping
  public ModelAndView getPlatforms() {
      ModelAndView mav = new ModelAndView("admin.platforms.list");
      mav.addObject("platforms", platformService.getAll());
      mav.addObject("pageTitle", "Plataformas");
      return mav;
  }
  
  @PostMapping
  public RedirectView savePlatform(@ModelAttribute("platform") Platform platform) {
      Platform savePlatform = new Platform();
      if(platform.getPlatformId() != null)
          savePlatform = platformService.getPlatformByPlatformId(platform.getPlatformId());
      savePlatform.setName(platform.getName());
      savePlatform.setDescription(platform.getDescription());
      savePlatform.setUrl(platform.getUrl());
      savePlatform.setUrlImagePlatform(platform.getUrlImagePlatform());
      platformService.updatePlatform(savePlatform);
      return new RedirectView("/admin/platforms");
  }
  
  @GetMapping("/create")
  public ModelAndView createPlatform() {
    
    ModelAndView mav = new ModelAndView("admin.platforms.edit");
    mav.addObject("platform", new Platform());
    mav.addObject("pageTitle", "Crear plataforma");
    return mav;
  }
  
  @GetMapping("/{platformId}")
  public ModelAndView getPlatform(@PathVariable("platformId") Long code) {
      
	  Platform platform = platformService.getPlatformByPlatformId(code);
	  
      ModelAndView mav = new ModelAndView("admin.platforms.edit");
      mav.addObject("platform", platform);
      mav.addObject("pageTitle", "Editar plataforma");
      return mav;
  }
  
  @DeleteMapping("/{platformId}")
  public RedirectView deleteCategory(@PathVariable("platformId") Long code) {
    
      Platform platform = platformService.getPlatformByPlatformId(code);
      
      platformService.deletePlatform(platform);
      return new RedirectView("/admin/platforms");
  }
}