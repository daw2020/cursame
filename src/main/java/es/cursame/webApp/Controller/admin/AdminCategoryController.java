package es.cursame.webApp.Controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Service.CategoryService;

@Controller // This means that this class is a Controller
@RequestMapping("/admin/categories")
public class AdminCategoryController implements AdminTemplateController {
  @Autowired
  private CategoryService categoryService;
  
  @GetMapping
  public ModelAndView getCategories() {
      ModelAndView mav = new ModelAndView("admin.categories.list");
      mav.addObject("categories", categoryService.getAll());
      mav.addObject("pageTitle", "Categorías");
      return mav;
  }
  
  @GetMapping("/create")
  public ModelAndView createCategory() {
    
    ModelAndView mav = new ModelAndView("admin.categories.edit");
    mav.addObject("category", new Category());
    mav.addObject("pageTitle", "Crear categoría");
    return mav;
  }
  
  @PostMapping
  public RedirectView saveCategory(@ModelAttribute("category") Category category) {
      Category saveCategory = new Category();
      if(category.getCategoryId() != null)
          saveCategory = categoryService.getCategoryByCategoryId(category.getCategoryId());
      saveCategory.setName(category.getName());
      saveCategory.setDescription(category.getDescription());
      categoryService.updateCategory(saveCategory);
      return new RedirectView("/admin/categories");
  }
  
  @GetMapping("/{categoryId}")
  public ModelAndView getCategory(@PathVariable("categoryId") Long code) {
      
	  Category category = categoryService.getCategoryByCategoryId(code);
	  
      ModelAndView mav = new ModelAndView("admin.categories.edit");
      mav.addObject("category", category);
      mav.addObject("pageTitle", "Editar categoría");
      return mav;
  }
  
  @DeleteMapping("/{categoryId}")
  public RedirectView deleteCategory(@PathVariable("categoryId") Long code) {
    
    Category category = categoryService.getCategoryByCategoryId(code);
    
    categoryService.deleteCategory(category);
    return new RedirectView("/admin/categories");
}
  
}