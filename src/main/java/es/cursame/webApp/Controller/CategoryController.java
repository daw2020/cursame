package es.cursame.webApp.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Service.CategoryService;

@Controller // This means that this class is a Controller
@RequestMapping("/categories")
public class CategoryController implements MainTemplateController {
  @Autowired
  private CategoryService categoryService;
  
  @GetMapping("/{categoryId}")
  public String getCourse(@PathVariable("categoryId") Long code, Model model) {
      
	  Category category = categoryService.getCategoryByCategoryId(code);
	  
      
      model.addAttribute("courses", category.getCourses());
      model.addAttribute("pageTitle", category.getName());
      return "courses.index";
  }
}