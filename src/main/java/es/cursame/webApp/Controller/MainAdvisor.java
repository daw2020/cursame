package es.cursame.webApp.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import es.cursame.webApp.DataTransferObjects.SearchForm;
import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Domain.Level;
import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Service.CategoryService;
import es.cursame.webApp.Service.PlatformService;

@ControllerAdvice(assignableTypes= {MainTemplateController.class})
public class MainAdvisor {
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private PlatformService platformService;
	
	@ModelAttribute("categories")
	public List<Category> getCategories() {
		return categoryService.getAll();
	}
	
	@ModelAttribute("platforms")
	public List<Platform> getPlatforms() {
		return platformService.getAll();
	}
	
	@ModelAttribute("levels")
	public Level[] getLevels() {
		return Level.values();
	}
	
	@ModelAttribute("searchForm")
	public SearchForm getSearchForm() {
	    return new SearchForm();
	}
}
