package es.cursame.webApp.Controller;

import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.cursame.webApp.Domain.Course;
import es.cursame.webApp.Domain.Platform;
import es.cursame.webApp.Domain.Remark;
import es.cursame.webApp.Domain.User;
import es.cursame.webApp.Service.CourseService;
import es.cursame.webApp.Service.RemarkService;
import es.cursame.webApp.Service.UserService;

@Controller // This means that this class is a Controller
@RequestMapping("/courses")
public class CourseController implements MainTemplateController {
  @Autowired
  private CourseService courseService;
  @Autowired
  private UserService userService;
  @Autowired
  private RemarkService remarkService;  
  
  @GetMapping("/{courseId}")
  public String getCourse(@PathVariable("courseId") Long code, Model model, Principal principal) {
	  Course courseCursame = null;
      if (code != null) {
    	  courseCursame = courseService.getCourseByCourseId(code);
      }
      
      List<Course> relatedCourses = courseCursame.getCategory().getCourses();
      relatedCourses.remove(courseCursame);
      
      User userLogged = userService.getUserByUsername(principal.getName());
      Platform coursePlatform = courseCursame.getPlatform();
      Remark courseRemark = new Remark("", 1, courseCursame, coursePlatform, userLogged);
      Integer courseScore = getScore(courseCursame);
      
      model.addAttribute("course", courseCursame);
      model.addAttribute("courseScore", courseScore); 
      model.addAttribute("relatedCourses", relatedCourses);
      model.addAttribute("remark", courseRemark);
      model.addAttribute("pageTitle", courseCursame.getTitle());
      return "courses.show";
  }
  
  private Integer getScore(Course courseCursame) {
	  Integer courseScore = 0;
      Integer totalRemarks = courseCursame.getRemarks().size();
      if(totalRemarks > 0) {
    	  for(Remark remark : courseCursame.getRemarks()) {
        	  courseScore += remark.getScore();
          }
          
          courseScore = Math.round(courseScore / courseCursame.getRemarks().size());
      }   
      
      return courseScore;
  }
  
  @GetMapping
  public String getAllCourses(Model model) {
	  model.addAttribute("courses", courseService.getAllCourses());
      model.addAttribute("pageTitle", "Todos los cursos");
      return "courses.index";	  
  }   
  
  @RequestMapping(value = "/{courseId}/remarks", method = RequestMethod.POST)  
  public String addRemark(@ModelAttribute("remark") Remark newRemark, BindingResult result) {
	  String[] suppressedFields = result.getSuppressedFields();
      if (suppressedFields.length > 0) {
          throw new RuntimeException("Attempting to bind disallowed fields: " + StringUtils.arrayToCommaDelimitedString(suppressedFields));
      }
	        
      remarkService.addRemark(newRemark);
      return "redirect:/courses/" + newRemark.getCourse().getCourseId();
  }
   
}