package es.cursame.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
//@ImportResource({ "classpath:webSecurityConfig.xml" })
@EnableWebSecurity
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {
    
    public SecSecurityConfig() {
        super();
    }
    
    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {       
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
        http.authorizeRequests()
            .antMatchers("/").permitAll()
            .antMatchers("/login*").permitAll()
            .antMatchers("/registration*").permitAll()
            .antMatchers("/forgotPassword*").permitAll()
            .antMatchers("/changePassword*").permitAll()     
            .antMatchers("/updatePassword*").permitAll()
            .antMatchers("/savePassword*").permitAll()
            .antMatchers("/privacy*").permitAll()
            .antMatchers("/thanks*").permitAll()
            .antMatchers( "/assets/**").permitAll()
            .antMatchers("/updatePassword*","/savePassword*").hasAuthority("CHANGE_PASSWORD_PRIVILEGE")
            .anyRequest().authenticated()
            
            .and()
            .formLogin()
            .loginPage("/login")
            .loginProcessingUrl("/login")
            .successHandler(myAuthenticationSuccessHandler())
            .failureUrl("/login?error=true")
            
            .and()
            .logout()            
            .deleteCookies("JSESSIONID")                                   
            
            .and()
            .rememberMe().key("uniqueAndSecret").tokenValiditySeconds(86400)
            
            .and()
            .csrf().disable()
            ;
        // @formatter:on
    }
    
    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler(){
        return new MyUrlAuthenticationSuccessHandler();
    }
      
}
