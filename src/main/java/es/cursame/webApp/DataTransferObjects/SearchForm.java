package es.cursame.webApp.DataTransferObjects;

import es.cursame.webApp.Domain.Category;
import es.cursame.webApp.Domain.Level;
import es.cursame.webApp.Domain.Platform;

public class SearchForm {
	private Category category;
	private Platform platform;
	private Level level;
	private String name;
	private String path;
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Platform getPlatform() {
		return platform;
	}
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
}
