INSERT INTO `category` (`category_id`, `description`, `name`) VALUES
(3, 'Programacion', 'Programacion'),
(4, 'Web design', 'Web design'),
(5, 'Bases de datos', 'Bases de datos'),
(6, 'Gestion de proyectos', 'Gestion de proyectos');

INSERT INTO `platform` (`platform_id`, `description`, `name`, `url`, `url_image_platform`) VALUES
(1, 'Udemy', 'Udemy', 'https://udemy.com', '/assets/img/logos/logo-udemy.svg'),
(2, 'YouTube', 'YouTube', 'https://www.youtube.com', '/assets/img/logos/logo-youtube.svg');

INSERT INTO `course` (`course_id`, `description`, `duration`, `image_url`, `level`, `payment_required`, `title`, `url`, `category_category_id`, `platform_platform_id`) VALUES
(1, 'Curso para beginners en web design', 4, '/assets/img/courses/1.jpg', 0, 0, 'Web design para principiantes', 'https://url.es', 4, 1),
(2, 'Curso de nivel intermedio de Spring con JSP', 4, '/assets/img/courses/2.jpg', 1, 0, 'JSP for Dummies', 'https://url.es', 3, 1),
(3, 'Para sacar el maximo jugo al Router que trae implementada la ultima version del framework Angular', 4, '/assets/img/angular.png', 2, 0, 'Usos avanzados del Router en Angular', 'https://url.es', 3, 1),
(4, 'Curso introductorio a los gestores de bases de datos NoSQL', 4, '/assets/img/mano.jpg', 0, 0, 'Iniciacion a las bases de datos NoSQL', 'https://url.es', 5, 2),
(5, 'Presentamos los fundamentos teoricos de una de las metodologias Agile mas utilizadas.', 4, '/assets/img/scrum.png', 0, 0, 'Gestion de desarrollos con SCRUM', 'https://url.es', 6, 2),
(6, 'Curso para profundizar en el lenguaje tipado de Microsoft', 6, '/assets/img/ts.png', 2, 0, 'TypeScript avanzado', 'https://url.es', 3, 1),
(7, 'Este curso detalla como orientar el uso de Node.js para intercomunicar dispositivos en el IoT', 2, '/assets/img/iot.jpg', 1, 0, 'Node.js para Internet of Things', 'https://url.es', 3, 1),
(8, 'Promesas, observables... todas las novedades de esta version del ECMAScript', 4, '/assets/img/es8.jpg', 1, 0, 'Novedades en ES8', 'https://url.es', 3, 1);

INSERT INTO `path` (`path_id`, `description`, `name`, `url_image_path`) VALUES
(1, 'Para aprender a programar', 'Programacion basica', '/assets/img/categories/1.jpg'),
(2, 'MongoDB, ExpressJS, Angular, Node.js', 'MEAN stack', '/assets/img/mean-stack.jpg'),
(3, 'Tecnologias del lado del cliente', 'Frontend', '/assets/img/frontend.jpg');

INSERT INTO `path_has_course` (`course_id`, `path_id`) VALUES
(1, 3),
(2, 1),
(3, 3),
(4, 2),
(5, 1),
(6, 3),
(7, 2),
(8, 3);

INSERT INTO role VALUES (1, 'ROLE_USER'), (2, 'ROLE_ADMIN');

INSERT INTO user VALUES (1, 'cursame@cursame.es', '$2a$10$yyp2..qw3odRkbUO8TaKTuZ/tD6KOO3mrG4SNsHeV69o3LkMYECE.', 'cursame'), (2, 'admin@cursame.es', '$2a$10$janwu7oYiEgVizCAzJuT8uAl9hj2UtfKp0kTqdoaPNXLfyOEAsW/u', 'admin');

INSERT INTO user_has_role VALUES(1,1), (2,2);

INSERT INTO remark VALUES(1, 'Excelente curso', 4, 3, 1, 1), (2, 'Demo de opinion', 2, 3, 1, 2);
